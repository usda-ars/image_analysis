#!/usr/bin/python
#
# processes.py
#
# LAST EDIT: 2017-06-21
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# IMPORT MODULES:
###############################################################################
import numpy
from scipy.misc import imresize
from scipy.ndimage import gaussian_filter
from scipy.ndimage.morphology import binary_dilation
from scipy.stats import pearsonr

from .dustutil import find_objects
from .dustutil import get_particle_sizes
from .imutil import calc_statistics
from .imutil import imopen
from .imutil import is_binary
from .imutil import is_rgb
from .imutil import otsu
from .imutil import rgb_to_gray
from .utilities import print_progress


__all__ = ['border',
           'binary_dilation',
           'crop',
           'dust_removal',
           'find_particle_size',
           'gaussian_filter',
           'merge',
           'mask',
           'scale_image',
           'stretch',
           'threshold',
           'union_masks']


###############################################################################
# FUNCTIONS
###############################################################################
def border(nd_array, boundary, bg_col=0):
    """
    Inputs:   - numpy.ndarray, binary image array (nd_array)
              - dict, boundary percentages (boundary)
              - [optional] bool/int, border color (bg_col)
                > 0/False .... black
                > 1/True ..... white
    Outputs:  numpy.ndarray
    Features: Colors the borders of an image
    """
    # Extract boundary percentages
    tpct = boundary.get('top', 0.0)
    bpct = boundary.get('bottom', 100.0)
    lpct = boundary.get('left', 0.0)
    rpct = boundary.get('right', 100.0)

    # Convert percentages to column/row indexes:
    h, w = nd_array.shape
    tidx = int(h*(tpct/100.0))
    lidx = int(w*(lpct/100.0))
    bidx = int(h*(bpct/100.0) - 1)
    ridx = int(w*(rpct/100.0) - 1)

    temp_array = nd_array.copy()
    if tidx > 0:
        temp_array[0:tidx, :] = bg_col
    if lidx > 0:
        temp_array[:, 0:lidx] = bg_col
    if bidx < h - 1:
        temp_array[bidx:h, :] = bg_col
    if ridx < w - 1:
        temp_array[:, ridx:w] = bg_col

    return temp_array


def crop(nd_array, boundary):
    """
    Inputs:   - numpy.ndarray, original image array (nd_array)
              - dict, boundary percentages (boundary)
    Outputs:  numpy.ndarray, cropped image array
    Features: Returns a cropped image array
    """
    # Extract boundary percentages
    tpct = boundary.get('top', 0.0)
    bpct = boundary.get('bottom', 100.0)
    lpct = boundary.get('left', 0.0)
    rpct = boundary.get('right', 100.0)

    # Convert percentages to column/row indexes:
    im_shape = nd_array.shape
    shape_len = len(im_shape)
    if shape_len == 3:
        h, w, d = im_shape
    elif shape_len == 2:
        h, w, = im_shape
        d = None
    tidx = int(h*(tpct/100.0))
    lidx = int(w*(lpct/100.0))
    bidx = int(h*(bpct/100.0) - 1)
    ridx = int(w*(rpct/100.0) - 1)

    if tidx >= 0 and tidx < bidx and bidx < h:
        if lidx >= 0 and lidx < ridx and ridx < w:
            if d is not None:
                tmp_array = nd_array[tidx:bidx, lidx:ridx, :].copy()
            else:
                tmp_array = nd_array[tidx:bidx, lidx:ridx].copy()
            return tmp_array
        else:
            raise IndexError("Left/right index out of range!")
    else:
        raise IndexError("Top/bottom index out of range!")


def dust_removal(my_array, dsize=None, verbose=False):
    """
    Name:     dust_removal
    Inputs:   - numpy.ndarray, binary image array (my_array)
              - [optional] int, dust size (dsize)
              - [optional] bool, whether to print statistics (verbose)
    Outputs:  numpy.ndarray, dusted binary image array
    Features: Removes pixel clusters from binary image smaller than the
              specified size
    Depends:  - calc_statistics
              - find_objects
              - find_particle_size
              - is_binary
    """
    if is_binary(my_array):
        # Find foreground pixel groups:
        my_array = my_array.astype("bool")

        # Quick process when no dust removal is requested:
        if dsize == 0:
            return my_array
        else:
            # Intelligently calculated dust particle size:
            if dsize is None:
                dsize = find_particle_size(my_array, 0.99)
                if verbose:
                    print("calculated dust particle size: %d" % (dsize))

            # Find dust particles:
            pixel_groups = find_objects(my_array)

            # Check the size of the pixel groups:
            sz_array = get_particle_sizes(pixel_groups)

            if verbose:
                size_stats = calc_statistics(sz_array)
                bin_count = numpy.bincount(sz_array)
                print("Group stats before:")
                print("  len: %d" % len(pixel_groups))
                print("  max: %d (n=%d)" % (
                    size_stats['max'], bin_count[size_stats['max']]))
                print("  min: %d (n=%d)" % (
                    size_stats['min'], bin_count[size_stats['min']]))
                print("  mean: %0.2f +/- %0.3f" % (
                    size_stats['ave'], size_stats['std']))
                print("  skew: %0.2f" % size_stats['skew'])
                print("  kurtosis: %0.2f" % size_stats['kurt'])

            # Remove dust:
            for group in pixel_groups:
                if group.size <= dsize:
                    # Consider this dust; remove it!
                    for point in group.points:
                        my_array[point] = False

            if verbose:
                pixel_groups = find_objects(my_array)
                sz_array = []
                for group in pixel_groups:
                    sz_array.append(group.size)
                sz_array = numpy.array(sz_array)
                size_stats = calc_statistics(sz_array)
                bin_count = numpy.bincount(sz_array)
                print("Group stats after:")
                print("  len: %d" % len(pixel_groups))
                print("  max: %d (n=%d)" % (
                    size_stats['max'], bin_count[size_stats['max']]))
                print("  min: %d (n=%d)" % (
                    size_stats['min'], bin_count[size_stats['min']]))
                print("  mean: %0.2f +/- %0.3f" % (
                    size_stats['ave'], size_stats['std']))
                print("  skew: %0.2f" % size_stats['skew'])
                print("  kurtosis: %0.2f" % size_stats['kurt'])

            return my_array
    else:
        raise TypeError("Image is not binary.")


def find_particle_size(my_array, corr_cutoff=0.95):
    """
    Name:     find_particle_size
    Inputs:   - numpy.ndarray, binary image (my_array)
              - [optional] float, correlation cutoff value (corr_cutoff)
    Outputs:  int, dust particle size
    Features: Returns the dust particle size based on the correlation
              coefficient between the original array and the array with
              dust particles filtered; when the correlation drops below a
              given cutoff value, return the previous particle size. If no
              particle size is found, return 0
    Depends:  - dust_removal
              - find_objects
    """
    my_particles = find_objects(my_array)
    my_part_sizes = []
    for my_particle in my_particles:
        my_part_sizes.append(my_particle.size)
    my_part_sizes = sorted(list(set(my_part_sizes)))
    num_part_sizes = len(my_part_sizes)
    for i in range(num_part_sizes):
        part_size = my_part_sizes[i]
        temp_array = my_array.copy()
        temp_array = dust_removal(temp_array, part_size)
        r, p = pearsonr(
            my_array.ravel().astype('uint8'),
            temp_array.ravel().astype('uint8'))
        if r < corr_cutoff:
            if i == 0:
                return my_part_sizes[0]
            else:
                return my_part_sizes[i - 1]
    return 0


def mask(orig_array, mask_array):
    """
    Applies a binary mask to a grayscale or color image.
    """
    orig_is_rgb = False
    if is_rgb(orig_array):
        orig_is_rgb = True
        h1, w1, d = orig_array.shape
    else:
        h1, w1 = orig_array.shape

    if not is_binary(mask_array):
        raise TypeError("Mask is not binary")
    else:
        h2, w2 = mask_array.shape

    if h1 != h2 or w1 != w2:
        raise IndexError("Mask array shape does not match original image!")

    midx = numpy.where(~mask_array.astype('bool'))
    r_array = numpy.copy(orig_array)

    if orig_is_rgb:
        R = orig_array[:, :, 0]
        G = orig_array[:, :, 1]
        B = orig_array[:, :, 2]
        R[midx] *= 0
        G[midx] *= 0
        B[midx] *= 0
        r_array[:, :, 0] = R
        r_array[:, :, 1] = G
        r_array[:, :, 2] = B
    else:
        # May fail unpacking shape here
        r_array[midx] *= 0

    return r_array


def merge(img_list, bg_black=True, verbose=True):
    """
    Merge grayscale/binary images together.
    """
    num_images = len(img_list)
    if num_images > 0:
        if verbose:
            print_progress(0, num_images, suffix="Merged")
        for i in range(num_images):
            img_file = img_list[i]
            img = imopen(img_file)
            if is_rgb(img):
                img = rgb_to_gray(img)

            if i == 0:
                merge_img = img.copy()
            else:
                if bg_black:
                    # Black background, merge white foreground
                    merge_img = numpy.maximum(merge_img, img)
                else:
                    # White background, merge black foreground
                    merge_img = numpy.minimum(merge_img, img)
            if verbose:
                print_progress(i+1, num_images, suffix="Merged")
        return merge_img
    else:
        raise ValueError("No images.")


def scale_image(nd_array, sfactor):
    """
    Name:     scale_image
    Inputs:   - numpy.ndarray, unscaled image array (nd_array)
              - int, scaling factor (sfactor)
    Returns:  numpy.ndarray, scaled image array
    Features: Returns a scaled numpy array
    Depends:  imresize
    """
    if sfactor != 100:
        try:
            # Try scaling factor as int:
            my_array = imresize(nd_array, sfactor)
        except:
            try:
                # Try scaling factor as float:
                sfactor = float(sfactor) / 100.0
                my_array = imresize(nd_array, sfactor)
            except:
                # Scaling failed, return original array:
                print("Scaling failed; returning original array")
                return nd_array
            else:
                return my_array
        else:
            return my_array
    else:
        # Skip scaling (not necessary)
        return nd_array


def stretch(nd_array, ml=None):
    """
    Name:     stretch
    Inputs:   - numpy.ndarray, 2D integer array (nd_array)
              - [optional] numpy.ndarray, boolean mask layer (ml)
    Returns:  numpy.ndarray, 2D integer array
    Features: Stretches the grayscale luminosity of the given image array
    Depends:  - crop
              - border
    """
    if not is_rgb(nd_array):
        temp_array = nd_array.copy().astype('float')

        # Get max/min values from region of interest:
        if ml is not None and isinstance(ml, numpy.ndarray):
            midx = numpy.where(ml.astype('bool'))
            try:
                max_val = nd_array[midx].max()
                min_val = nd_array[midx].min()
            except ValueError:
                # failed to stretch, maybe image is too dark/bright
                return nd_array
            else:
                dif_val = max_val - min_val
                temp_array[midx] -= float(min_val)
                temp_array[midx] *= 255.0
                temp_array[midx] /= float(dif_val)
        else:
            try:
                max_val = nd_array.max()
                min_val = nd_array.min()
            except ValueError:
                return nd_array
            else:
                dif_val = max_val - min_val
                temp_array -= float(min_val)
                temp_array *= 255.0
                temp_array /= float(dif_val)

        return temp_array.astype('uint8')
    else:
        print("Stretching failed; returning original array")
        return nd_array


def threshold(nd_array, boundary=None, fg_white=True):
    """
    Name:     threshold
    Inputs:   - numpy.ndarray, 2D integer array (nd_array)
              - [optional] dict, boundary extents (boundary)
              - [optional] bool, whether foreground color is white (fg_white)
    Outputs:  numpy.ndarray, two-dimensional boolean array (my_binary)
    Features: Returns a binary image based on the Otsu (1979) method
    Depends:  - crop
              - otsu
    """
    if fg_white:
        # Light foreground on black background
        l_max = 1
        l_min = 0
    else:
        # Dark foreground on light background
        l_max = 0
        l_min = 1

    if boundary is not None and isinstance(boundary, dict):
        temp_array = crop(nd_array, boundary)
        thresh = otsu(temp_array.astype('uint8'))
    else:
        thresh = otsu(nd_array.astype('uint8'))

    # Create a copy of the given array in float32 type:
    my_binary = numpy.copy(nd_array).astype("f4")

    # Find the foreground and background pixel values:
    # NOTE: foreground is assumed [thresh ... 255]
    #       background is assumed [0 ... thresh]
    fore_idx = numpy.where(my_binary > thresh)
    back_idx = numpy.where(my_binary <= thresh)

    # Correct foreground and background based on user preference:
    my_binary[fore_idx] *= 0
    my_binary[fore_idx] += 1
    my_binary[fore_idx] *= l_max

    my_binary[back_idx] *= 0
    my_binary[back_idx] += 1
    my_binary[back_idx] *= l_min

    # Return byte array where roots are 1 and background is 0:
    return my_binary.astype("bool")


def union_masks(mask1, mask2):
    """
    Name:     union_masks
    Inputs:   - numpy.ndarray, boolean mask #1 (mask1)
              - numpy.ndarray, boolean mask #2 (mask2)
    Outputs:  numpy.ndarray, boolean array union of masks #1 and #2
    Features: Returns the union where both image mask arrays are true
    Depends:  is_binary
    """
    if is_binary(mask1) and is_binary(mask2):
        if mask1.shape == mask2.shape:
            mask3 = (mask1 == mask2) & mask1
            return mask3
        else:
            raise IndexError("Mask arrays shapes do not match")
    else:
        raise TypeError("Masks must be binary arrays")


###############################################################################
# MAIN
###############################################################################
if __name__ == '__main__':
    import os

    from .imutil import rgb_to_b

    my_border = {'top': 37, 'right': 64, 'bottom': 70.5, 'left': 32}
    my_scale = 25
    my_gauss = None
    my_dust = None
    my_iter = 2

    home_dir = os.path.expanduser("~")
    work_dir = os.path.join(home_dir, "Desktop", 'temp')
    my_file = os.path.join(work_dir, "Ye478day7_level2_001.tiff")

    base_array = imopen(my_file)
    base_array = scale_image(base_array, my_scale)
    mask_array = base_array.copy()

    mask_array = rgb_to_b(mask_array)
    if my_gauss is not None:
        mask_array = gaussian_filter(mask_array.astype("float"), my_gauss)
    mask_array = stretch(mask_array)
    mask_array = threshold(mask_array)
    mask_array = border(mask_array, my_border)

    # -------------------------------------------------------------------------
    # Analyze dust:
    my_particles = find_objects(mask_array)
    my_part_sizes = []
    for my_particle in my_particles:
        my_part_sizes.append(my_particle.size)
    my_part_sizes = sorted(list(set(my_part_sizes)))
    num_part_sizes = len(my_part_sizes)
    corr_cutoff = 0.95
    for i in range(num_part_sizes):
        part_size = my_part_sizes[i]
        temp_array = mask_array.copy()
        temp_array = dust_removal(temp_array, part_size)
        r, p = pearsonr(
            mask_array.ravel().astype('uint8'),
            temp_array.ravel().astype('uint8'))
        if r < corr_cutoff:
            if i == 0:
                print("particle size: {}".format(my_part_sizes[0]))
                break
            else:
                print("particle size: {}".format(my_part_sizes[i - 1]))
                break

    # -------------------------------------------------------------------------

    mask_array = dust_removal(mask_array, dsize=my_dust)
    if my_iter is not None:
        mask_array = binary_dilation(
            mask_array.astype('bool'), iterations=my_iter)
    out_array = mask(base_array, mask_array)
    out_array = rgb_to_gray(out_array)
    out_array = stretch(out_array, ml=mask_array)
    out_array = threshold(out_array)
