#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# utilities.py
#
# LAST EDIT: 2017-06-21
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# IMPORT MODULES:
###############################################################################
import glob
import os
import sys

__all__ = ['angle_to_index',
           'create_dir',
           'dir_has_images',
           'find_image_files',
           'is_number',
           'print_messages',
           'print_progress']


###############################################################################
# PARAMETER DEFINITIONS:
###############################################################################
# Currently support image types:
img_types = (
    ".jpg", ".JPG", ".jpeg",
    ".tif", ".tiff",
    ".gif", ".GIF",
    ".png", ".PNG",
    ".nef", ".NEF")


###############################################################################
# FUNCTIONS
###############################################################################
def angle_to_index(idx, angle, count):
    """
    Name:     angle_to_index
    Inputs:   - int, current image index (idx)
              - float, separation angle (angle)
              - int, total count of images in sequence (count)
    Outputs:
    Features: Returns the image index for a given angle assuming that the
              number of images are associated with an imaging sequence.
    """
    if not isinstance(angle, (int, float)):
        raise TypeError("Angle must be a number")
    if not isinstance(count, int) or count < 0:
        raise TypeError("Image count must be a positive integer")
    if not isinstance(idx, int):
        raise TypeError("Image index must be an integer")
    if idx < 0 or idx >= count:
        raise IndexError("Image index out of range (%d-%d)" % (0, count - 1))
    if count < 2:
        raise ValueError("Image count must be greater than one")
    elif count == 2 and angle not in (0, 180, 360):
        raise ValueError("Angle mis-match for two-image sequence")
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
    # NOTE: this type of checking could go on forever...; however,        #
    #       I have chosen to only check for 0, 1 and 2 image counts       #
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    # Calculate the inter-photo angle (deg/photo):
    ipa = 360.0/count

    # Convert inter-photo angle to increment and round to nearest whole number:
    dx_est = angle/ipa
    dx = int(dx_est + 0.5)

    # Calculate the angle error due to rounding:
    dx_err = abs(dx - dx_est)
    ang_err = dx_err*ipa

    # Find the index associated with the delta increment, dx;
    # index is wrapped to beginning when it exceeds count
    nxt = idx + dx
    if nxt > (count - 1):
        nxt -= count
        assert nxt < count

    return (nxt, ang_err)


def create_dir(my_dir):
    """
    Name:     create_dir
    Inputs:   str, directory (my_dir)
    Outputs:  bool, success
    Features: Attempts to create a directory on the user's computer
    """
    is_done = False
    if not os.path.isdir(my_dir):
        try:
            os.mkdir(my_dir)
        except:
            is_done = False
        else:
            is_done = True
    else:
        # Directory already exists!
        is_done = True

    return is_done


def dir_has_images(my_dir):
    """
    Name:     dir_has_images
    Inputs:   str, directory (my_dir)
    Outputs:  bool
    Features: Returns boolean if given directory contains image files
    Depends:  find_image_files
    """
    my_list = find_image_files(my_dir)
    if len(my_list) > 0:
        return True
    else:
        return False


def find_image_files(my_dir):
    """
    Name:     find_image_files
    Inputs:   str, directory name (my_dir)
    Outputs:  list, image paths
    Features: Returns a sorted list of image files found in a given directory
    Depends:  img_types (global variable)
    """
    my_files = []
    for img_type in img_types:
        s_str = os.path.join(my_dir, "*%s" % (img_type))
        my_files += glob.glob(s_str)
    my_files = sorted(list(set(my_files)))

    return my_files


def is_number(s):
    """
    Name:     is_number
    Inputs:   str, test string (s)
    Outputs:  tuple, (bool, float)
    Features: Tests if a string is a number
    """
    try:
        a = float(s)
    except ValueError:
        return (False, None)
    else:
        return (True, a)


def print_messages(msg_list, char_count, prefix=None):
    """
    Name:     print_messages
    Inputs:   - list, error messages (msg_list)
              - int, character fill line length (char_count)
              - [optional] str, character to precede each message in the list;
                if left None, the list will be consecutively numbered
    Outputs:  None.
    Features: Prints formatted messages
    """
    for i in range(len(msg_list)):
        # Break each error message into individual words:
        msg = msg_list[i].split(" ")

        # Split the error message into separate lines based on each
        # line's length
        out_lines = []
        line_num = 0
        out_lines.append("")
        for j in range(len(msg)):
            count = len(out_lines[line_num] + msg[j])
            if count > char_count - 4:
                line_num += 1
                out_lines.append(msg[j])
            else:
                out_lines[line_num] += msg[j]
            out_lines[line_num] += " "
        for k in range(len(out_lines)):
            if not out_lines[k].isspace() and out_lines[k] != '':
                if k == 0 and prefix is None:
                    print("{0:2}. {1:}".format(i + 1, out_lines[k]))
                elif k == 0:
                    print("{0:>3.2} {1:}".format(prefix, out_lines[k]))
                else:
                    print("    {}".format(out_lines[k]))
    print("{}".format('-'*char_count))


def print_progress(iteration, total, prefix='', suffix='', decimals=0,
                   bar_length=44):
    """
    Name:     print_progress
    Inputs:   - int, current iteration (iteration)
              - int, total iterations (total)
              - [optional] str, prefix string (prefix)
              - [optional] str, suffix string (suffix)
              - [optional] int, decimal number in percent complete (decimals)
              - [optional] int, character length of bar (bar_length)
    Outputs:  None.
    Features: Creates a terminal progress bar
    Reference: "Python Progress Bar" by aubricus
    https://gist.github.com/aubricus/f91fb55dc6ba5557fbab06119420dd6a
    """
    str_format = "{0:." + str(decimals) + "f}"
    percents = str_format.format(100 * (iteration / float(total)))
    filled_length = int(round(bar_length * iteration / float(total)))
    bar = '█' * filled_length + '-' * (bar_length - filled_length)

    sys.stdout.write(
        '\r%s |%s| %s%s %s' % (prefix, bar, percents, '%', suffix)),

    if iteration == total:
        sys.stdout.write('\n')
    sys.stdout.flush()
