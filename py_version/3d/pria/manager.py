#!/usr/bin/python
#
# manager.py
#
# LAST EDIT: 2017-06-27
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# USAGE:
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# > python setup.py [flag [value]]
# Optional flags are described in MANAGER's params dictionary (keys).
# Note that the flags/values used here are the same as those written to file
# using the 'w' command.
# For batch processing, pass the -w -e -q flags at the end of setup.py to
# save the parameters to file, execute the processing and quit when finished.
#
###############################################################################
# IMPORT MODULES:
###############################################################################
import os
import re
import time

from .imutil import imopen
from .imutil import imsave
from .imutil import rgb_to_b
from .imutil import rgb_to_gray
from .imutil import rgb_to_max
from .imutil import rgb_to_min
from .imutil import show_image
from .processes import border
from .processes import binary_dilation
from .processes import dust_removal
from .processes import gaussian_filter
from .processes import mask
from .processes import merge
from .processes import scale_image
from .processes import stretch
from .processes import threshold
from .processes import union_masks
from .utilities import create_dir
from .utilities import dir_has_images
from .utilities import find_image_files
from .utilities import is_number
from .utilities import print_messages
from .utilities import print_progress


###############################################################################
# CLASSES
###############################################################################
class MANAGER(object):
    """
    Name:    MANAGER
    History: Version 0.9.0
             - updated help text for run option [17.06.27]
             - made rawpy an optional dependency [17.06.21]
             Version 0.8.1
             - fixed failed imports during setup [17.06.21]
             - fixed stretch mask in execute [17.06.16]
             Version 0.8.0
             - replaced positional mask with delta mask [17.05.17]
             Version 0.7.0
             - created mask union process function [17.05.17]
             - moved dir has images function to utilities [17.05.16]
             - corrected to stretch boolean option [17.05.16]
             - added create positional mask function [17.05.16]
             - added create color mask function [17.05.16]
             - created option 'a' for pos mask angle [17.05.15]
             - created options '1' and '2' for color/position masks [17.05.15]
             Version 0.6.1
             - fixed crash on 'h' [17.05.08]
             - added time print out to execute [17.04.17]
             - added stretch process [17.04.17]
             - fixed border indexing [17.04.17]
             - added error-handling to mask function [17.04.17]
             - created intelligent dust particle calculator [17.04.17]
             Version 0.5
             - added scaling to show merge [17.04.12]
             - removed redundant working dir write to param file [17.04.11]
             - sorted arg list during write to param file [17.04.11]
             - added show flag to param dict to hide options [17.04.11]
             - added hidden state variable to by-pass run() [17.04.11]
             - added quit func to param dict [17.04.11]
             - moved create dir to utilities [17.04.10]
             - created execute option -e [17.04.10]
             - split process images into start process and execute [17.04.10]
             - execute attempts to automatically create output dir [17.04.10]
             Version 0.4
             - write params no longer require manager to be running [17.04.10]
             - removed run command from class init [17.04.10]
             - moved to pipr pacakge [17.04.10]
             Version 0.3
             - added optional args to assign functions [17.04.10]
             - parse args now operates on assing functions [17.04.10]
             - created get functions for parameter writing [17.04.10]
             - created assign, prompt and set output dir functions [17.04.10]
             - created params dictionary [17.04.10]
             - updated show and do functions for new dict [17.04.10]
             - changed parameter option for parameter table to -t [17.04.10]
             - changed parameter option for parameter list to -p [17.04.10]
             Version 0.2
             - allow border percentages to be floats [17.04.07]
             - changed parameter option for working dir to -d [17.04.07]
             - changed parameter option for dust size to -p [17.04.07]
             - changed parameter option for scaling factor to -x [17.04.07]
             - separated set functions into set, assign and prompt [17.04.07]
             - created check val function [17.04.07]
             - created parameter file [17.04.07]
             - created write parameter option [17.04.07]
             Version 0.1
             - allow gaussian stdev value of None to skip blur [17.04.06]
             - allow iters value of None to skip dilation [17.04.06]
             - added is number function [17.04.06]
             - added regular expression for help option parsing [17.04.06]
             - added help functions [17.04.06]
             - added background/foreground color option to threshold [17.04.06]
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, args=None):
        # State variables:
        self.is_okay = True       # goes false when an error is encountered
        self.is_running = False   # to gracefully exit from run()
        self._hidden_state = 0    # to by-pass run() with command line -q

        # Processing parameters:
        self.work_dir = None      # working directory (has images)
        self.output_dir = None    # output directory (for saving images)
        self.black_bg = True      # boolean if background is black else white
        self.dilate_iter = 3      # number of iterations for dilating binary
        self.dust_sz = None       # threshold for dust filtering
        self.gauss_sd = 2         # standard deviation for blur/smoothing
        self.im_extents = {'top': 0, 'bottom': 100, 'left': 0, 'right': 100}
        self.sfactor = 100        # scaling factor (to reduce processing)
        self.to_stretch = False   # stretch blue channel luminance
        self.color_mask = True    # mask yellow from image sequence
        self.delta_mask = False   # range to min pixel value mask

        # Parameter output file (i.e., command-line arguments + values):
        self.param_file = "prip.param"

        # Help string regular expression matching pattern:
        self.h_pattern = re.compile("^h(elp)?\(([a-z|0-9]{1})\)")

        # The display image:
        self.im_merge = None
        self.im_set = []

        # The parameter options:
        self.params = {
            '1': {
                'name': 'COLOR MASK',
                'text': 'color mask',
                'type': 'process',
                'show': True,
                'func': self.assign_color_mask,
                'dump': self.get_color_mask,
                'help': ('Set to True to run the color mask, which attempts '
                         'to remove yellow from the image sequence. '
                         'The color mask is created based on the following '
                         'processing steps: '
                         '(1) extract blue color band from RGB image; '
                         '(2) blur/smooth image based on Gaussian standard '
                         'deviation; '
                         '(3) stretch blurred pixel range to 0-255; '
                         '(3) threshold image (Otsu method); '
                         '(4) apply the border to binary image; '
                         '(5) apply the dust filter; and '
                         '(6) dilate the binary image to create the mask.')},
            '2': {
                'name': 'DELTA MASK',
                'text': 'delta mask',
                'type': 'process',
                'show': True,
                'func': self.assign_delta_mask,
                'dump': self.get_delta_mask,
                'help': ('Set to True to run the delta mask, which '
                         'compares the range of pixel values in an RGB '
                         'image to their minium value.'
                         'The delta mask is created based on the following '
                         'processing steps: '
                         '(1) calculate the max/min pixels from RGB image; '
                         '(2) calculate the range array (max - min); '
                         '(3) threshold min and range arrays (Otsu method); '
                         '(4) create the boolean mask (where min != range); '
                         '(5) apply the border to binary image; '
                         '(6) apply the dust filter; and '
                         '(7) dilate the binary image to create the mask.')},
            'q': {
                'name': 'QUIT',
                'text': 'quit',
                'type': 'option',
                'show': False,
                'func': self.quit,
                'dump': None,
                'help': 'Use this command any time to exit the program.'},
            'h': {
                'name': 'HELP',
                'text': '',
                'type': 'option',
                'show': False,
                'func': None,
                'dump': None,
                'help': 'Displays the help message for a given option.'},
            'b': {
                'name': 'BORDER EXTENTS',
                'text': 'border extents',
                'type': 'process',
                'show': True,
                'func': self.assign_border,
                'dump': self.get_border,
                'help': ('Your image region of interest is within the border '
                         'boundaries. Border boundaries are prescribed as the '
                         'relative percentages of the pixel dimensions, where '
                         'the top-left corner of your image is located at 0%, '
                         'and the bottom-right corner of your image is '
                         'located at 100%. Use the merge ("m") command to see '
                         'how your boundary extents fit your region of '
                         'interest.')},
            'c': {
                'name': 'COLOR OF BACKGROUND',
                'text': 'color of background',
                'type': 'process',
                'show': True,
                'func': self.assign_bg_color,
                'dump': self.get_bg_color,
                'help': ('This defines whether the background color is '
                         'lighter (white) or darker (black) than your '
                         'foreground image. The processed images will be '
                         'converted such that foreground is white and '
                         'background is black.')},
            'd': {
                'name': 'WORKING DIRECTORY',
                'text': 'working directory',
                'type': 'process',
                'show': True,
                'func': self.assign_working_dir,
                'dump': self.get_working_dir,
                'help': ('This is the directory on you computer where the '
                         'sequence of images to be processed is located. For '
                         'processing, a sub-directory called "out" will be '
                         'created here to store processed images (image file '
                         'names are not changed).')},
            'e': {
                'name': 'EXECUTE',
                'text': None,
                'type': 'option',
                'show': False,
                'func': self.execute,
                'dump': None,
                'help': ('Executes image processing; automatically creates '
                         'the output directory "out" in the working directory '
                         'if the output directory is not already defined. '
                         'Pass this flag at the end of your command-line '
                         'arguments to execute the image processing. Make '
                         'certain you pass all the parameter flags ahead of '
                         'this one; execute fails if -d is not passed and '
                         'output directory is not defined.')},
            'g': {
                'name': 'GAUSSIAN STANDARD DEV',
                'text': 'gaussian standard deviation',
                'type': 'process',
                'show': True,
                'func': self.assign_gauss_sd,
                'dump': self.get_gauss_sd,
                'help': ('The standard deviation for the multi-dimensional '
                         'Gaussian filter, which determines the kernel size '
                         'for processing. Used in the color mask only. '
                         'Increasing this value will result '
                         'in blurring the image across a larger area. This '
                         'helps reduce the static noise that is commonly '
                         'present in images taken under low-light conditions. '
                         'Note that downscaling the image resolution '
                         'essentially performs the same task; therefore, this '
                         'feature is not recommended when scaling down '
                         'images.')},
            'i': {
                'name': 'ITERATIONS OF DILATION',
                'text': 'iterations of dilation',
                'type': 'process',
                'show': True,
                'func': self.assign_dilation,
                'dump': self.get_dilation,
                'help': ('The number of dilation iterations performed on a '
                         'binary mask. The binary mask is defined as the '
                         'foreground features found during the image '
                         'processing. Increasing the number of dilation '
                         'iterations will expand the mask to include more '
                         'foreground in the final processed images.')},
            'l': {
                'name': 'LUMINANCE STRETCH',
                'text': 'luminance stretch',
                'type': 'process',
                'show': True,
                'func': self.assign_stretch,
                'dump': self.get_stretch,
                'help': ('Stretches the luminance of an array '
                         'to make the max and min pixel values range from '
                         '0 to 255. Used in the color mask by default and '
                         'optionally on the masked luminance array. '
                         'This may improve the contrast between '
                         'the foreground and background.')},
            'm': {
                'name': 'MERGE IMAGE SEQUENCE',
                'text': 'merge image sequence',
                'type': 'option',
                'show': True,
                'func': self.show_merge,
                'dump': None,
                'help': ('Display an overlay of all the images found in the '
                         'working directory. Images are converted to '
                         'grayscale (luminance method) with foreground '
                         'features merged based on the current specified '
                         'background color. This feature requires additional '
                         'software for visualizing the merged image (e.g., '
                         'http://www.imagemagick.org). The defined border '
                         'boundary and scaling factor preference are applied '
                         'to the merged image.')},
            'o': {
                'name': 'OUTPUT DIRECTORY',
                'text': 'output directory',
                'type': 'process',
                'func': self.assign_output_dir,
                'show': False,
                'dump': None,
                'help': ('This is the directory on you computer where the '
                         'sequence of processed images are saved. By '
                         'default, the output directory is the sub-directory '
                         '"out" located in the working directory.')},
            'p': {
                'name': 'PROCESSING PARAMETERS',
                'text': 'parameter options',
                'type': 'option',
                'show': True,
                'func': self.show_parameter_options,
                'dump': None,
                'help': 'Prints a list of the parameter setting options.'},
            'r': {
                'name': 'RUN IMAGE PROCESSING',
                'text': 'run image processing',
                'type': 'option',
                'show': True,
                'func': self.start_image_process,
                'dump': None,
                'help': ('The image processing steps for each image are: '
                         '(1) resize image based on scaling factor; '
                         '(2) create a color mask (if selected); '
                         '(3) create a delta mask (if selected); '
                         '(4) overlay mask(s) onto color image; '
                         '(5) grayscale masked RGB image (luminance method); '
                         '(6) stretch grayscale range (if selected); '
                         '(7) convert grayscale image to binary; '
                         '(8) save binary image to output directory.')},
            's': {
                'name': 'DUST PARTICLE SIZE',
                'text': 'dust particle size',
                'type': 'process',
                'show': True,
                'func': self.assign_dust_size,
                'dump': self.get_dust_size,
                'help': ('There may be specks present in the binary images '
                         'after processing. Increasing the dust particle size '
                         'will remove larger specks from the processed image; '
                         'however, it may also result in the removal of small '
                         'pixel clusters from your foreground.')},
            't': {
                'name': 'VALUE TABLE',
                'text': 'table of values',
                'type': 'option',
                'show': True,
                'func': self.show_parameter_values,
                'dump': None,
                'help': 'Prints a list of the current parameter values.'},
            'w': {
                'name': 'WRITE TO FILE',
                'text': 'write parameters to file',
                'type': 'option',
                'show': True,
                'func': self.write_params,
                'dump': None,
                'help': ('Writes the current working diretory and parameter '
                         'list to file that may be read as a command-line '
                         'argument for running this program.')},
            'x': {
                'name': 'SCALING FACTOR PERCENT',
                'text': 'scaling factor percent',
                'type': 'process',
                'show': True,
                'func': self.assign_scale,
                'dump': self.get_scale,
                'help': ('The percent scaling factor resizes the images '
                         'before the image processing steps are performed. By '
                         'reducing the image size, some processing steps may '
                         'complete quicker.')}
        }

        if args is not None and isinstance(args, list):
            self.parse_args(args)

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Property Definitions
    # ////////////////////////////////////////////////////////////////////////
    @property
    def merge_img(self):
        """
        Pixel array of merged image sequence.
        """
        if self.im_merge is None:
            try:
                self.im_merge = merge(self.im_set, bg_black=self.black_bg)
            except:
                self.is_okay = False
                self.warn("Failed to merge")
            else:
                return self.im_merge
        else:
            return self.im_merge

    @property
    def num_images(self):
        """
        Total number of image files in the image set.
        """
        return len(self.im_set)

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Function Definitions
    # ////////////////////////////////////////////////////////////////////////
    def assign_bg_color(self, val=None):
        """
        Name:     MANAGER.assign_bg_color
        Inputs:   [optional] str, input value (val)
        Outputs:  bool/NoneType
        Features: Requests a new background color (white or black);
                  attempts to assign a value if it is not None
        Depends:  - prompt_bg_color
                  - request
                  - set_bg_color
        """
        if val is not None:
            return self.set_bg_color(val)
        else:
            self.request("Assign the image background color")
            is_done = False
            while not is_done:
                is_done = self.prompt_bg_color()

    def assign_border(self, val=None):
        """
        Name:     MANAGER.assign_border
        Inputs:   [optional] str, input value (val)
        Outputs:  bool/NoneType
        Features: Assigns the border percentages
        Note:     This border is NOT used in the thresholding---it seems to
                  cause as many problems as it solves---rather, it is applied
                  after thresholding (and before dust removal) to block out
                  unwanted parts of the image
        Depends:  - prompt_border
                  - request
        """
        if val is not None:
            # Parse key:val syntax
            p = val.split(":")
            if len(p) == 2:
                k, v = p
                return self.set_border(k, v)
            else:
                return False
        else:
            self.request("Assign border extent percentages (0--100)")
            for key in sorted(list(self.im_extents)):
                is_done = False
                if self.is_running:
                    while not is_done:
                        is_done = self.prompt_border(key)

    def assign_dilation(self, val=None):
        """
        Name:     MANAGER.assign_dilation
        Inputs:   [optional] str, input value (val)
        Outputs:  bool/NoneType
        Features: Assigns the number of dilation iterations used in the blue
                  mask (to expand the boundary around the binary blue image);
                  a value less than 1 will repeat iterations until the result
                  no longer changes; set to 'None' to skip processing step
        Depends:  - prompt_dilation
                  - request
        """
        if val is not None:
            return self.set_dilation(val)
        else:
            self.request("Set dilation iterations")
            is_done = False
            while not is_done:
                is_done = self.prompt_dilation()

    def assign_dust_size(self, val=None):
        """
        Name:     MANAGER.assign_dust_size
        Inputs:   [optional] str, input value (val)
        Outputs:  bool/NoneType
        Features: Assigns the minimum dust particle size for removal; set to
                  'None' to skip processing step
        Depends:  - prompt_dust_size
                  - request
        """
        if val is not None:
            return self.set_dust_size(val)
        else:
            self.request("Set dust particle size for removal")
            is_done = False
            while not is_done:
                is_done = self.prompt_dust_size()

    def assign_gauss_sd(self, val=None):
        """
        Name:     MANAGER.assign_gauss_sd
        Inputs:   [optional] str, input value (val)
        Outputs:  bool/NoneType
        Features: Assigns the Gaussian standard deviation scalar for blurring
                  the input blue band/grayscale image; set value to 'None' to
                  skip this processing step
        Depends:  - prompt_gauss_sd
                  - request
        """
        if val is not None:
            return self.set_gauss_sd(val)
        else:
            self.request("Set Gaussian standard devation for smoothing")
            is_done = False
            while not is_done:
                is_done = self.prompt_gauss_sd()

    def assign_output_dir(self):
        """
        Name:     MANAGER.assign_output_dir
        Inputs:   None.
        Outputs:  None.
        Features: Requests and sets the output directory
        Depends:  - create_dir
                  - prompt_output_dir
                  - request
                  - warn
        TODO:     _ user selection 'o' does not allow them to change the
                    output directory
        """
        if self.output_dir is None:
            if self.work_dir is not None:
                temp_dir = os.path.join(self.work_dir, "out")
                if not os.path.isdir(temp_dir):
                    prompt = "Set output directory to '%s' (y/n)? " % temp_dir
                    ans = input(prompt)
                    if ans.lower() == 'y' or ans.lower() == 'yes':
                        # Create output directory
                        self.is_okay = create_dir(temp_dir)
                        if self.is_okay:
                            self.output_dir = temp_dir
                        else:
                            self.warn("Failed to create output directory")
                    else:
                        self.request("Enter the directory for saving output")
                        is_done = False
                        while not is_done:
                            is_done = self.prompt_output_dir()
                else:
                    self.output_dir = temp_dir
            else:
                self.is_okay = False
                self.warn("No working directory defined")

    def assign_scale(self, val=None):
        """
        Name:     MANAGER.assign_scale
        Inputs:   [optional] str, input value (val)
        Outputs:  bool/NoneType
        Features: Requests and assigns the image scaling factor, which is
                  applied before image processing
        Depends:  - prompt_scale
                  - request
                  - set_scale
        """
        if val is not None:
            return self.set_scale(val)
        else:
            self.request("Set the scaling factor percentage (0--100)")
            is_done = False
            while not is_done:
                is_done = self.prompt_scale()

    def assign_stretch(self, val=None):
        """
        Name:     MANAGER.assign_stretch
        Inputs:   [optional] str, input value (val)
        Outputs:  bool/NoneType
        Features: Requests and assigns the image stretch boolean, which is
                  applied to the bordered blue grayscale array
        Depends:  - prompt_stretch
                  - request
                  - set_stretch
        """
        if val is not None:
            return self.set_stretch(val)
        else:
            self.request("Set stretch boolean")
            is_done = False
            while not is_done:
                is_done = self.prompt_stretch()

    def assign_color_mask(self, val=None):
        """
        Name:     MANAGER.assign_color_mask
        Inputs:   [optional] str, input value (val)
        Outputs:  bool/NoneType
        Features: Requests and assigns the boolean of whether to run the
                  color mask
        Depends:  - prompt_color_mask
                  - request
                  - set_color_mask
        """
        if val is not None:
            return self.set_color_mask(val)
        else:
            self.request("Set color mask boolean")
            is_done = False
            while not is_done:
                is_done = self.prompt_color_mask()

    def assign_delta_mask(self, val=None):
        """
        Name:     MANAGER.assign_delta_mask
        Inputs:   [optional] str, input value (val)
        Outputs:  bool/NoneType
        Features: Requests and assigns the boolean of whether to run the
                  delta mask
        Depends:  - prompt_delta_mask
                  - request
                  - set_delta_mask
        """
        if val is not None:
            return self.set_delta_mask(val)
        else:
            self.request("Set delta mask boolean")
            is_done = False
            while not is_done:
                is_done = self.prompt_delta_mask()

    def assign_working_dir(self, val=None):
        """
        Name:     MANAGER.assign_working_dir
        Inputs:   [optional] str, input value (val)
        Outputs:  bool/NoneType
        Features: Requests and assigns the working directory
        Depends:  - request
                  - prompt_working_dir
        """
        if val is not None:
            return self.set_working_dir(val)
        else:
            self.request("Enter the directory for processing")
            is_done = False
            while not is_done:
                is_done = self.prompt_working_dir()

    def check_ans(self, val):
        """
        Name:     MANAGER.check_ans
        Inputs:   str, user's answer to prompt (val)
        Outputs:  bool, user's confirmation
        Features: Requests confirmation on user input
        """
        prompt = "You entered '%s', is this correct (y/n)? " % (val)
        ans = input(prompt)
        if ans.lower() == 'y' or ans.lower() == 'yes':
            return True
        else:
            return False

    def check_val(self, val):
        """
        Name:     MANAGER.check_val
        Inputs:   str, value (val)
        Outputs:  bool, whether quit or skip option was entered
        Features: Checks answer for the quit and skip options
        Depends:  quit
        """
        if val.lower() == 'q' or val.lower() == 'quit':
            is_done = True
            self.quit()
        elif val.isspace() or val == '':
            # Skip parameter setting
            is_done = True
        else:
            is_done = False

        return is_done

    def create_color_mask(self, nd_array):
        """
        Name:     MANAGER.create_color_mask
        Inputs:   numpy.ndarray, RGB color image array (nd_array)
        Outputs:  numpy.ndarray, boolean mask array
        Features: Creates a boolean array mask to remove yellow pixels from
                  an image based on user's options
        Depends:  - binary_dilation
                  - border
                  - dust_removal
                  - gaussian_filter
                  - rgb_to_b
                  - stretch
                  - threshold
        """
        # Work on a copy of the array data:
        mask_array = nd_array.copy()

        # ---------------------------------------------------------------------
        # RGB TO BLUE
        # extract blue channel in order to eliminate yellows from the mesh
        # ---------------------------------------------------------------------
        mask_array = rgb_to_b(mask_array)

        # ---------------------------------------------------------------------
        # BLUR
        # smooth out salt-and-pepper static noise that commonly is present
        # in the blue channel
        # ---------------------------------------------------------------------
        if self.gauss_sd is not None:
            mask_array = gaussian_filter(
                mask_array.astype("float"), self.gauss_sd)

        # ---------------------------------------------------------------------
        # STRETCH BLUE
        # stretch blue channel values to fill the full range;
        # also fixes over-filled pixels from Gaussian filter
        # (i.e., pixel values > 255)
        # ---------------------------------------------------------------------
        mask_array = stretch(mask_array)

        # ---------------------------------------------------------------------
        # BLUE TO BINARY
        # binarize blue channel
        # NOTE: send threshold function the image extents for
        # region-of-interest thresholding; it tends to result in more broken
        # roots and less noisy artefacts
        # ---------------------------------------------------------------------
        fg_white = False
        if self.black_bg:
            fg_white = True
        mask_array = threshold(mask_array, boundary=None, fg_white=fg_white)

        # ---------------------------------------------------------------------
        # BLOCKOUT BORDER
        # overlay the border around the binary image
        # ---------------------------------------------------------------------
        mask_array = border(mask_array, self.im_extents)

        # ---------------------------------------------------------------------
        # REMOVE DUST
        # filter out small self-standing pixel clusters
        # NOTE: dust_sz == None will intelligently calculate the dust size
        # based on the correlation coefficient between the original and
        # dust-filtered arrays
        # ---------------------------------------------------------------------
        mask_array = dust_removal(mask_array, dsize=self.dust_sz)

        # ---------------------------------------------------------------------
        # DILATE
        # expand the binary mask in order to capture more of the surrounding
        # color pixels
        # ---------------------------------------------------------------------
        if self.dilate_iter is not None:
            mask_array = binary_dilation(
                mask_array.astype('bool'), iterations=self.dilate_iter)

        return mask_array.astype('bool')

    def create_delta_mask(self, nd_array):
        """
        Name:     MANAGER.create_delta_mask
        Inputs:   numpy.ndarray, RGB color image array (nd_array)
        Outputs:  numpy.ndarray, boolean mask array
        Features: Creates a boolean array mask to remove the background from
                  a root image based on user's options
        Depends:  - binary_dilation
                  - border
                  - dust_removal
                  - rgb_to_max
                  - rgb_to_min
                  - threshold
        """
        # ---------------------------------------------------------------------
        # RGB TO GRAYSCALE
        # create the max, min and delta grayscale arrays
        # ---------------------------------------------------------------------
        cmax = rgb_to_max(nd_array)
        cmin = rgb_to_min(nd_array)
        delta = (cmax - cmin)

        # ---------------------------------------------------------------------
        # CONVERT TO BINARY
        # binarize min and delta arrays
        # NOTE: send threshold function the image extents for
        # region-of-interest thresholding; it tends to result in more broken
        # roots and less noisy artefacts
        # ---------------------------------------------------------------------
        fg_white = False
        if self.black_bg:
            fg_white = True
        cmin_bin = threshold(cmin, boundary=None, fg_white=fg_white)
        delta_bin = threshold(delta, boundary=None, fg_white=fg_white)

        # ---------------------------------------------------------------------
        # CREATE MASK
        # cmin highlights roots over background
        # delta highlights mesh over roots
        # create mask where cmin is True and not equal to delta
        # ---------------------------------------------------------------------
        mask_array = (cmin_bin != delta_bin) & cmin_bin

        # ---------------------------------------------------------------------
        # BLOCKOUT BORDER
        # overlay the border around the binary image
        # ---------------------------------------------------------------------
        mask_array = border(mask_array, self.im_extents)

        # ---------------------------------------------------------------------
        # REMOVE DUST
        # filter out small self-standing pixel clusters
        # NOTE: dust_sz == None will intelligently calculate the dust size
        # based on the correlation coefficient between the original and
        # dust-filtered arrays
        # ---------------------------------------------------------------------
        mask_array = dust_removal(mask_array, dsize=self.dust_sz)

        # ---------------------------------------------------------------------
        # DILATE
        # expand the binary mask in order to capture more of the surrounding
        # color pixels
        # ---------------------------------------------------------------------
        if self.dilate_iter is not None:
            mask_array = binary_dilation(
                mask_array.astype('bool'), iterations=self.dilate_iter)

        return mask_array.astype('bool')

    def do_next(self):
        """
        Name:     MANAGER.do_next
        Inputs:   None.
        Outputs:  None.
        Features: Checks user response for next processing step
        """
        ans = input("(h for help) > ")
        # Catch non-argument quit keywords:
        if ans.lower() == 'exit' or ans.lower() == 'quit':
            self.quit()
        elif ans.lower().find('h') == 0:
            self.help(ans)
        elif ans.lower() in self.params:
            self.params[ans]['func']()
        else:
            self.show_options()

    def execute(self):
        """
        Name:     MANAGER.execute
        Inputs:   None.
        Outputs:  None.
        Features: Executes image processing steps
        Depends:  - create_color_mask     - create_delta_mask
                  - imopen                - imsave
                  - mask                  - print_progress
                  - rgb_to_gray           - scale_image
                  - stretch               - threshold
                  - union_masks           - warn
        TODO:     _ still apply the border when both masks are set to false
        """
        if self.work_dir is not None:
            is_ready = False
            if self.output_dir is None:
                # Automatically try to create an output directory:
                temp_dir = os.path.join(self.work_dir, "out")
                is_ready = create_dir(temp_dir)
                if is_ready:
                    self.output_dir = temp_dir
            else:
                is_ready = True

            if is_ready:
                start_time = time.time()
                print_progress(0, self.num_images, suffix="Processed")
                for i in range(self.num_images):
                    im_file = self.im_set[i]
                    im_basename = os.path.basename(im_file)
                    im_out_path = os.path.join(self.output_dir, im_basename)

                    try:
                        base_array = imopen(im_file)
                    except:
                        self.is_okay = False
                        self.warn("Failed to open image")
                    else:
                        # -----------------------------------------------------
                        # SCALE
                        # drop image resolution for faster processing
                        # -----------------------------------------------------
                        out_array = scale_image(base_array, self.sfactor)

                        # -----------------------------------------------------
                        # APPLY MASKS
                        # overlay binary mask(s) to color image, if specified
                        # union the two masks if both are selected
                        # -----------------------------------------------------
                        cmask = None
                        dmask = None
                        if self.color_mask:
                            cmask = self.create_color_mask(out_array)
                        if self.delta_mask:
                            dmask = self.create_delta_mask(out_array)

                        imask = None
                        if cmask is not None and dmask is not None:
                            # Union of pixels that are True in both masks:
                            imask = union_masks(cmask, dmask)
                        elif cmask is not None and dmask is None:
                            imask = cmask
                        elif dmask is not None and cmask is None:
                            imask = dmask

                        if imask is not None:
                            out_array = mask(out_array, imask)

                        # -----------------------------------------------------
                        # CONVERT TO GRAYSCALE
                        # add back info from red and gren color bands to the
                        # grayscale
                        # -----------------------------------------------------
                        out_array = rgb_to_gray(out_array)

                        # -----------------------------------------------------
                        # STRETCH LUMINANCE
                        # stretch the luminance values to fill the full range
                        # NOTE: send stretch function the layer mask to stretch
                        # only the new grayscale pixels
                        # -----------------------------------------------------
                        if self.to_stretch:
                            out_array = stretch(out_array, ml=imask)

                        # -----------------------------------------------------
                        # CONVERT TO BINARY
                        # binarize the masked grayscale image
                        # -----------------------------------------------------
                        out_array = threshold(out_array)

                        # -----------------------------------------------------
                        # WRITE TO FILE
                        # -----------------------------------------------------
                        if os.path.isdir(self.output_dir):
                            imsave(im_out_path, out_array)

                        print_progress(
                            i+1, self.num_images, suffix="Processed")
                end_time = time.time()
                diff_time = end_time - start_time
                print("(finished executing in %g seconds)" % (diff_time))
            else:
                self.warn("Cannot execute; output directory not set")
        else:
            self.warn("Cannot execute; working directory not set")

    def get_bg_color(self, key):
        """
        Name:     MANAGER.get_bg_color
        Inputs:   str, parameter key (key)
        Outputs:  str, key-value pair
        Features: Returns the command line argument for the current background
                  color
        """
        if self.black_bg:
            my_col = 'black'
        else:
            my_col = 'white'
        line = "-{} {}".format(key, my_col)
        return line

    def get_border(self, key):
        """
        Name:     MANAGER.get_border
        Inputs:   str, parameter key (key)
        Outputs:  str, key-value pair
        Features: Returns the command line argument for the current border
                  values
        """
        line = (
            "-{option} top:{top} "
            "-{option} right:{right} "
            "-{option} bottom:{bottom} "
            "-{option} left:{left}").format(
                option=key,
                top=self.im_extents['top'],
                right=self.im_extents['right'],
                left=self.im_extents['left'],
                bottom=self.im_extents['bottom'])
        return line

    def get_color_mask(self, key):
        """
        Name:     MANAGER.get_color_mask
        Inputs:   str, parameter key (key)
        Outputs:  str, key-value pair
        Features: Returns the command line argument for running the color mask
        """
        line = "-{} {}".format(key, self.color_mask)
        return line

    def get_delta_mask(self, key):
        """
        Name:     MANAGER.get_delta_mask
        Inputs:   str, parameter key (key)
        Outputs:  str, key-value pair
        Features: Returns the command line argument for running the delta mask
        """
        line = "-{} {}".format(key, self.delta_mask)
        return line

    def get_dilation(self, key):
        """
        Name:     MANAGER.get_dilation
        Inputs:   str, parameter key (key)
        Outputs:  str, key-value pair
        Features: Returns the command line argument for the current number of
                  dilation iterations
        """
        line = "-{} {}".format(key, self.dilate_iter)
        return line

    def get_dust_size(self, key):
        """
        Name:     MANAGER.get_dust_size
        Inputs:   str, parameter key (key)
        Outputs:  str, key-value pair
        Features: Returns the command line argument for the current dust
                  particle size for filtering
        """
        line = "-{} {}".format(key, self.dust_sz)
        return line

    def get_gauss_sd(self, key):
        """
        Name:     MANAGER.get_gauss_sd
        Inputs:   str, parameter key (key)
        Outputs:  str, key-value pair
        Features: Returns the command line argument for the current Gaussian
                  standard deviation value
        """
        line = "-{} {}".format(key, self.gauss_sd)
        return line

    def get_scale(self, key):
        """
        Name:     MANAGER.get_scale
        Inputs:   str, parameter key (key)
        Outputs:  str, key-value pair
        Features: Returns the command line argument for the current scaling
                  factor
        """
        line = "-{} {}".format(key, self.sfactor)
        return line

    def get_stretch(self, key):
        """
        Name:     MANAGER.get_stretch
        Inputs:   str, parameter key (key)
        Outputs:  str, key-value pair
        Features: Returns the command line argument for the current stretch
                  boolean
        """
        line = "-{} {}".format(key, self.to_stretch)
        return line

    def get_working_dir(self, key):
        """
        Name:     MANAGER.get_working_dir
        Inputs:   str, parameter key (key)
        Outputs:  str, key-value pair
        Features: Returns the command line argument for the current working
                  directory
        """
        line = "-{} {}".format(key, self.work_dir)
        return line

    def help(self, h_str):
        """
        Name:     MANAGER.help
        Inputs:   str, help request string (h_str)
        Outputs:  None.
        Features: Checks the user's help request based on the prescribed help
                  string pattern
        Depends:  show_help
        """
        # Match the help string against the defined pattern:
        m = self.h_pattern.match(h_str)

        if m is None and h_str.lower() == 'h':
            print("Usage: h(<option>)")
        elif m is None and h_str.lower() == 'help':
            print("Usage: help(<option>)")
        elif m is not None:
            self.show_help(m.groups()[1])
        else:
            self.show_options()

    def load_image_set(self, my_dir):
        """
        Name:     MANAGER.load_image_set
        Inputs:   str, directory (my_dir)
        Outputs:  None.
        Features: Reads a given directory for image files
        """
        if os.path.isdir(my_dir):
            self.im_set = find_image_files(my_dir)

        # Set okay flag to False for empty directories:
        if self.num_images > 0:
            self.is_okay = True
        else:
            self.is_okay = False

    def parse_args(self, arg_list):
        """
        Name:     MANAGER.parse_args
        Inputs:   list, list of command line arguments (arg_list)
        Outputs:  None.
        Features: Reads the command line arguments for class properties
        Depends:  - load_image_set
                  - warn
        """
        valid_args = ["-{}".format(i) for i in self.params]
        noval_args = [
            str(i) for i in self.params if self.params[i]['type'] == 'option']
        num_args = len(arg_list)
        if num_args > 1:
            for i in range(num_args):
                arg = arg_list[i]
                # Check for valid argument:
                if arg in valid_args:
                    arg = arg.lstrip("-")
                    f = self.params[arg]['func']
                    # Check for valid function:
                    if f is not None and arg not in noval_args:
                        # Check for argument value (i.e., -arg value):
                        try:
                            val = arg_list[i+1]
                        except IndexError:
                            self.warn("No value passed for '%s'" % (arg))
                        else:
                            is_set = f(val)
                            if not is_set:
                                print("Failed to set -{} {}".format(arg, val))
                    elif f is not None and arg in noval_args:
                        # Execute command argument function:
                        f()
                    else:
                        print(arg)

    def prompt_bg_color(self):
        """
        Name:     MANAGER.prompt_bg_color
        Inputs:   None.
        Outputs:  bool, successful assignment
        Features: Prompts user for background color
        Depends:  - check_val
                  - set_bg_color
                  - warn
        """
        if self.black_bg:
            prompt = "background (current value: 'black')> "
        else:
            prompt = "background (current value: 'white')> "

        val = input(prompt)
        is_done = self.check_val(val)
        if not is_done:
            is_done = self.set_bg_color(val)
            if not is_done:
                self.warn("Background color must be 'black' or 'white'")

        return is_done

    def prompt_border(self, key):
        """
        Name:     MANAGER.prompt_border
        Inputs:   str, border name (key)
        Outputs:  bool, successful assignment
        Features: Prompts user for border values
        Depends:  - check_val
                  - set_border
                  - warn
        """
        prompt = "%s (current value: %s%%)> " % (key, self.im_extents[key])
        val = input(prompt)
        is_done = self.check_val(val)
        if not is_done:
            is_done = self.set_border(key, val)
            if not is_done:
                self.warn(
                    "Border percentage must be a number between "
                    "0 and 100")

        return is_done

    def prompt_color_mask(self):
        """
        Name:     MANAGER.prompt_color_mask
        Inputs:   None.
        Outputs:  bool, successful assignment
        Features: Prompts user for color mask boolean
        Depends:  - check_val
                  - set_color_mask
                  - warn
        """
        prompt = "color mask (current value: %s)> " % (self.color_mask)
        val = input(prompt)
        is_done = self.check_val(val)
        if not is_done:
            is_done = self.set_color_mask(val)
            if not is_done:
                self.warn("Color mask value must be true (1) or false (0)")

        return is_done

    def prompt_delta_mask(self):
        """
        Name:     MANAGER.prompt_delta_mask
        Inputs:   None.
        Outputs:  bool, successful assignment
        Features: Prompts user for delta mask boolean
        Depends:  - check_val
                  - set_delta_mask
                  - warn
        """
        prompt = "delta mask (current value: %s)> " % (self.delta_mask)
        val = input(prompt)
        is_done = self.check_val(val)
        if not is_done:
            is_done = self.set_delta_mask(val)
            if not is_done:
                self.warn("Delta mask value must be true (1) or false (0)")

        return is_done

    def prompt_dilation(self):
        """
        Name:     MANAGER.prompt_dilation
        Inputs:   None.
        Outputs:  bool, successful assignment
        Features: Prompts user for dilation iteration value
        Depends:  - check_val
                  - set_dilation
                  - warn
        """
        prompt = "dilation iters (current value: %s)> " % self.dilate_iter
        val = input(prompt)
        is_done = self.check_val(val)
        if not is_done:
            is_done = self.set_dilation(val)
            if not is_done:
                self.warn(
                    "Dilation iterations must be an integer greater than 0")

        return is_done

    def prompt_dust_size(self):
        """
        Name:     MANAGER.prompt_dust_size
        Inputs:   None.
        Outputs:  bool, successful assignment
        Features: Prompts user for a dust particle size
        Depends:  - check_val
                  - set_dust_size
                  - warn
        """
        prompt = "dust size (current value: %s)> " % self.dust_sz
        val = input(prompt)
        is_done = self.check_val(val)
        if not is_done:
            is_done = self.set_dust_size(val)
            if not is_done:
                self.warn(
                    "Dust particle size must be an integer greater than 0")

        return is_done

    def prompt_gauss_sd(self):
        """
        Name:     MANAGER.prompt_gauss_sd
        Inputs:   None.
        Outputs:  bool, successful assignment
        Features: Prompts user for a value
        Depends:  - check_val
                  - set_gauss_sd
                  - warn
        """
        prompt = "stdev (current value: %s)> " % (self.gauss_sd)
        val = input(prompt)
        is_done = self.check_val(val)
        if not is_done:
            is_done = self.set_gauss_sd(val)
            if not is_done:
                self.warn("Standard deviation must be a number greater than 0")

        return is_done

    def prompt_output_dir(self):
        """
        Name:     MANAGER.prompt_output_dir
        Inputs:   None.
        Outputs:  bool, successful assignment
        Features: Prompts user for an output directory
        Depends:  - check_ans
                  - check_val
                  - set_output_dir
                  - warn
        """
        prompt = "> "
        ans = input(prompt)
        is_done = self.check_val(ans)
        if not is_done:
            if self.check_ans(ans):
                is_done = self.set_output_dir(ans)
                if not is_done:
                    self.warn("Output directory must exist")

        return is_done

    def prompt_scale(self):
        """
        Name:     MANAGER.prompt_scale
        Inputs:   None.
        Outputs:  bool, successful assignment
        Features: Prompts user for scale factor
        Depends:  - check_val
                  - set_scale
                  - warn
        """
        prompt = "scale (current value: %s%%)> " % (self.sfactor)
        val = input(prompt)
        is_done = self.check_val(val)
        if not is_done:
            is_done = self.set_scale(val)
            if not is_done:
                self.warn(
                    "Scaling factor must be a number between 0 and 100")

        return is_done

    def prompt_stretch(self):
        """
        Name:     MANAGER.prompt_stretch
        Inputs:   None.
        Outputs:  bool, successful assignment
        Features: Prompts user for stretch boolean
        Depends:  - check_val
                  - set_stretch
                  - warn
        """
        prompt = "stretch (current value: %s)> " % (self.to_stretch)
        val = input(prompt)
        is_done = self.check_val(val)
        if not is_done:
            is_done = self.set_stretch(val)
            if not is_done:
                self.warn("Stretch value must be true (1) or false (0)")

        return is_done

    def prompt_working_dir(self):
        """
        Name:     MANAGER.prompt_working_dir
        Inputs:   None.
        Outputs:  bool, successful assignment
        Features: Prompts user for a working directory
        Depends:  - check_ans
                  - check_val
                  - set_working_dir
                  - warn
        """
        prompt = "working dir (%s)> " % self.work_dir
        ans = input(prompt)
        is_done = self.check_val(ans)
        if not is_done:
            if self.check_ans(ans):
                is_done = self.set_working_dir(ans)
                if not is_done and os.path.isdir(ans):
                    self.warn("No images found in directory")
                elif not is_done:
                    self.warn("Working directory must exist")

        return is_done

    def quit(self):
        """
        Name:     MANAGER.quit
        Inputs:   None.
        Outputs:  None.
        Features: Terminates the run sequence
        """
        self.is_running = False
        self._hidden_state = -1
        print("Exiting...")

    def request(self, msg):
        """
        Name:     MANAGER.request
        Inputs:   str, message (msg)
        Outputs:  None.
        Features: Format prints a request to user
        """
        msg_len = len(msg)
        print("-"*msg_len)
        print(msg)
        print("-"*msg_len)

    def reset(self):
        """
        Name:     MANAGER.reset
        Inputs:   None.
        Outputs:  None.
        Features: Resets critical properties for new sequence processing
        """
        self.im_merge = None
        self.output_dir = None

    def run(self):
        """
        Name:     MANAGER.run
        Inputs:   None.
        Outputs:  None.
        Features: Main class run loop
        Depends:  - do_next
                  - assign_working_dir
                  - show_options
        """
        self.is_running = True
        if self._hidden_state == 0:
            if self.work_dir is None:
                self.assign_working_dir()
            if self.is_running:
                self.show_options()
                while self.is_okay and self.is_running:
                    self.do_next()

    def set_bg_color(self, val):
        """
        Name:     MANAGER.set_bg_color
        Inputs:   str, input value
        Outputs:  bool, success
        Features: Sets the black background color boolean
        """
        if val.lower() == 'white' or val.lower() == '1':
            is_done = True
            self.black_bg = False
        elif val.lower() == 'black' or val.lower() == '0':
            is_done = True
            self.black_bg = True
        else:
            is_done = False

        return is_done

    def set_border(self, key, val):
        """
        Name:     MANAGER.set_border
        Inputs:   - str, border name (key)
                  - str, border value (val)
        Outputs:  bool, success
        Features: Sets the border extent value
        Depends:  is_number
        """
        is_done = False
        if key in self.im_extents:
            check, val = is_number(val)
            if check:
                if val >= 0 and val <= 100:
                    is_done = True
                    self.im_extents[key] = val

        return is_done

    def set_color_mask(self, val):
        """
        Name:     MANAGER.set_color_mask
        Inputs:   str, color mask boolean (val)
        Outputs:  bool, success
        Features: Sets the color mask boolean
        """
        is_done = False
        if val.lower() == 'true' or val.lower() == '1':
            is_done = True
            self.color_mask = True
        elif val.lower() == 'false' or val.lower() == '0':
            is_done = True
            self.color_mask = False

        return is_done

    def set_delta_mask(self, val):
        """
        Name:     MANAGER.set_delta_mask
        Inputs:   str, delta mask boolean (val)
        Outputs:  bool, success
        Features: Sets the delta mask boolean
        """
        is_done = False
        if val.lower() == 'true' or val.lower() == '1':
            is_done = True
            self.delta_mask = True
        elif val.lower() == 'false' or val.lower() == '0':
            is_done = True
            self.delta_mask = False

        return is_done

    def set_dilation(self, val):
        """
        Name:     MANAGER.set_dilation
        Inputs:   str, value (val)
        Outputs:  bool, successful assignment
        Features: Sets dilation iteration value
        """
        is_done = False
        if val.lower() == "none":
            # Allow value to be unset:
            is_done = True
            self.dilate_iter = None
        else:
            try:
                val = int(val)
            except:
                pass
            else:
                if val >= 0:
                    is_done = True
                    self.dilate_iter = val

        return is_done

    def set_dust_size(self, val):
        """
        Name:     MANAGER.set_dust_size
        Inputs:   str, dust particle size (val)
        Outputs:  bool, success
        Features: Sets the dust particle size for filtering
        """
        is_done = False
        if val.lower() == "none":
            # Allow value to be unset:
            is_done = True
            self.dust_sz = None
        else:
            try:
                val = int(val)
            except:
                pass
            else:
                if val >= 0:
                    is_done = True
                    self.dust_sz = val

        return is_done

    def set_gauss_sd(self, val):
        """
        Name:     MANAGER.set_gauss_sd
        Inputs:   str, value (val)
        Outputs:  bool, successful assignment
        Features: Sets the Gaussian standard deviation value
        Depends:  is_number
        """
        is_done = False
        if val.lower() == "none":
            # Allow value to be unset:
            is_done = True
            self.gauss_sd = None
        else:
            check, val = is_number(val)
            if check:
                if val > 0:
                    is_done = True
                    self.gauss_sd = val

        return is_done

    def set_output_dir(self, val):
        """
        Name:     MANAGER.set_output_dir
        Inputs:   str, output directory (val)
        Outputs:  bool, success
        Features: Sets the output directory for saving processed image files
        """
        is_done = False
        if os.path.isdir(val):
            is_done = True
            self.output_dir = val

        return is_done

    def set_scale(self, val):
        """
        Name:     MANAGER.set_scale
        Inputs:   str, scale factor (val)
        Outputs:  bool, success
        Features: Sets the image scale factor
        Depends:  is_number
        """
        is_done = False
        check, val = is_number(val.rstrip("%"))
        if check:
            if val > 0 and val <= 100:
                is_done = True
                # Quick round to nearest whole number:
                self.sfactor = int(val + 0.5)

        return is_done

    def set_stretch(self, val):
        """
        Name:     MANAGER.set_stretch
        Inputs:   str, stretch boolean (val)
        Outputs:  bool, success
        Features: Sets the image stretch boolean
        """
        is_done = False
        if val.lower() == 'true' or val.lower() == '1':
            is_done = True
            self.to_stretch = True
        elif val.lower() == 'false' or val.lower() == '0':
            is_done = True
            self.to_stretch = False

        return is_done

    def set_working_dir(self, val):
        """
        Name:     MANAGER.set_working_dir
        Inputs:   str, working directory (val)
        Outputs:  bool, success
        Features: Sets the working directory
        """
        is_done = False
        if os.path.isdir(val) and dir_has_images(val):
            is_done = True
            self.reset()
            self.load_image_set(val)
            self.work_dir = val
            print("(read %d files in working directory)" % self.num_images)

        return is_done

    def show_help(self, h_opt):
        """
        Name:     MANAGER.show_help
        Inputs:   str, help option (h_opt)
        Outputs:  None.
        Features: Prints help messages for a given help option
        Depends:  print_messages
        """
        if h_opt in self.params:
            msg_text = "{}: {}".format(self.params[h_opt]['name'],
                                       self.params[h_opt]['help'])
            msg = [msg_text, ]
        else:
            msg = [("ERROR: Option '%s' not recognized." % (h_opt)), ]

        print_messages(msg, 79, '>>')

    def show_merge(self):
        """
        Name:     MANAGER.show_merge
        Inputs:   None.
        Outputs:  None.
        Features: Displays the merged image set to test the image extents
        Depends:  - border
                  - scale_image
                  - show_image
        """
        if self.black_bg:
            bg_col = 0
        else:
            bg_col = 1
        try:
            my_merge = border(self.merge_img, self.im_extents, bg_col)
        except:
            self.is_okay = False
            self.warn("Merge failed")
        else:
            # Scale, if necessary:
            if self.sfactor != 100:
                my_merge = scale_image(my_merge.copy(), self.sfactor)
            show_image(my_merge)

    def show_options(self):
        """
        Name:     MANAGER.show_options
        Inputs:   None.
        Outputs:  None.
        Features: Prints user options
        Depends:  request
        """
        self.request("Select an option for processing")
        for arg in sorted(list(self.params.keys())):
            param_type = self.params[arg]['type']
            to_show = self.params[arg]['show']
            if param_type == 'option' and to_show:
                line = "{} ...... {}".format(arg, self.params[arg]['text'])
                print(line)
        line = "{} ...... {}".format('q', self.params['q']['text'])
        print(line)

    def show_parameter_options(self):
        """
        Name:     MANAGER.show_parameter_options
        Inputs:   None.
        Outputs:  None.
        Features: Prints processing parameter options
        Depends:  request
        """
        self.request("Select option to set parameter")
        for arg in sorted(list(self.params.keys())):
            param_type = self.params[arg]['type']
            to_show = self.params[arg]['show']
            if param_type == 'process' and to_show:
                line = "{} ...... {}".format(arg, self.params[arg]['text'])
                print(line)
        line = "{} ...... {}".format('q', self.params['q']['text'])
        print(line)

    def show_parameter_values(self):
        """
        Name:     MANAGER.show_parameter_values
        Inputs:   None.
        Outputs:  None.
        Features: Prints table of current processing parameter values
        Depends:  request
        """
        if self.black_bg:
            bg_color = "black"
        else:
            bg_color = "white"

        my_table_vals = {
            0: ("Image count ", self.num_images),
            1: ("Background ", bg_color),
            2: ("Iterations ", self.dilate_iter),
            3: ("Dust size ", self.dust_sz),
            4: ("Gaussian stdev ", self.gauss_sd),
            5: ("Scale percent ", self.sfactor),
            6: ("Stretching", self.to_stretch),
            7: ("Border extents:", ''),
            8: ("> top ", self.im_extents['top']),
            9: ("> right ", self.im_extents['right']),
            10: ("> bottom ", self.im_extents['bottom']),
            11: ("> left ", self.im_extents['left']),
            12: ("Image masks:", ''),
            13: ("1. color", self.color_mask),
            14: ("2. delta", self.delta_mask),
        }

        self.request("Processing parameter values")
        for i in range(len(my_table_vals)):
            name, val = my_table_vals[i]
            if val == '':
                line = "{}".format(name)
            else:
                line = "{:.<20} {}".format(name, val)
            print(line)

    def start_image_process(self):
        """
        Name:     MANAGER.start_image_process
        Inputs:   None.
        Outputs:  None.
        Features: Starts the workflow for processing images from the new 3D
                  setup
        Depends:  - assign_output_dir
                  - assign_working_dir
                  - execute
        """
        if self.work_dir is None and self.is_running:
            self.assign_working_dir()

        if self.output_dir is None and self.is_running:
            self.assign_output_dir()

        if self.is_okay and self.is_running:
            self.execute()

    def warn(self, msg):
        """
        Name:     MANAGER.warn
        Inputs:   str, warning message (msg)
        Outputs:  None.
        Features: Prints a warning message to user
        """
        print("!!! %s !!!" % msg)

    def write_params(self):
        """
        Name:     MANAGER.write_params
        Inputs:   None.
        Outputs:  None.
        Features: Writes to file the key-value pairs of the command-line
                  arguments for the current processing parameters
        Depends:  warn
        """
        if self.work_dir is not None:
            out_path = os.path.join(self.work_dir, self.param_file)

            try:
                f = open(out_path, 'w')
            except IOError:
                self.warn("Could not open parameter file for writing")
            else:
                out_str = ""
                for arg in sorted(list(self.params.keys())):
                    if self.params[arg]['type'] == 'process':
                        if self.params[arg]['dump'] is not None:
                            line = self.params[arg]['dump'](arg)
                            out_str += "{} ".format(line)
                f.write(out_str)
                f.close()
        else:
            self.warn("Working directory is not set")

###############################################################################
# MAIN PROGRAM
###############################################################################
if __name__ == '__main__':
    # DEFINE DIRECTORIES:
    home_dir = os.path.expanduser("~")
    base_dir = os.path.join(
        home_dir, "Desktop", "temp", "img", "Session-1")

    proc_dir = os.path.join(base_dir, "out")

    proc_dir0 = os.path.join(base_dir, "blue")
    proc_dir1 = os.path.join(base_dir, "blur")
    proc_dir2 = os.path.join(base_dir, "bluebin")
    proc_dir3 = os.path.join(base_dir, "bluedust")
    proc_dir4 = os.path.join(base_dir, "bluedile")
    proc_dir5 = os.path.join(base_dir, "colormask")

    # TEST BOUNDARIES
    if False:
        my_merge = merge(find_image_files(proc_dir1))
        im_bound = {'top': 37, 'bottom': 90, 'left': 16, 'right': 80}
        my_merge = border(my_merge, im_bound)
        show_image(my_merge)

    # DO ALL:
    if False:
        im_sf = 75
        im_bound = {'top': 37, 'bottom': 90, 'left': 16, 'right': 80}
        for img_file in find_image_files(base_dir):
            img_basename = os.path.basename(img_file)
            img_out_path = os.path.join(proc_dir, img_basename)

            base_array = imopen(img_file)
            base_array = scale_image(base_array, im_sf)
            temp_array = base_array.copy()

            temp_array = rgb_to_b(temp_array)               # RGB TO BLUE
            temp_array = gaussian_filter(     # BLUR
                temp_array.astype("float"), 2)
            temp_array = threshold(temp_array)              # BLUE TO BINARY
            temp_array = border(temp_array, im_bound)       # BLOCKOUT BORDER
            temp_array = dust_removal(temp_array, dsize=2)  # RM DUST
            temp_array = binary_dilation(
                temp_array.astype('bool'), iterations=3)    # DILATE

            out_array = mask(base_array, temp_array)
            out_array = rgb_to_gray(out_array)
            if os.path.isdir(proc_dir):
                imsave(img_out_path, out_array)

    # RGB TO BLUE
    if False:
        for img_file in find_image_files(base_dir):
            img_basename = os.path.basename(img_file)
            img_out_path = os.path.join(proc_dir0, img_basename)

            im_array = imopen(img_file)
            b_array = rgb_to_b(im_array)
            if os.path.isdir(proc_dir0):
                imsave(img_out_path, b_array)

    # BLUR
    if False:
        for img_file in find_image_files(proc_dir0):
            img_basename = os.path.basename(img_file)
            img_out_path = os.path.join(proc_dir1, img_basename)

            im_array = imopen(img_file)
            b_array = gaussian_filter(
                im_array.astype("float"), 2)
            if os.path.isdir(proc_dir1):
                imsave(img_out_path, b_array)

    # BLUE TO BINARY:
    if False:
        for img_file in find_image_files(proc_dir1):
            img_basename = os.path.basename(img_file)
            img_out_path = os.path.join(proc_dir2, img_basename)

            im_array = imopen(img_file)
            im_bound = {'top': 37, 'bottom': 90, 'left': 16, 'right': 80}
            b_array = threshold(im_array)
            b_array = border(b_array, im_bound)
            if os.path.isdir(proc_dir2):
                imsave(img_out_path, b_array)

    # DUST REMOVAL
    if False:
        for img_file in find_image_files(proc_dir2):
            img_basename = os.path.basename(img_file)
            img_out_path = os.path.join(proc_dir3, img_basename)

            im_array = imopen(img_file)
            b_array = dust_removal(im_array, dsize=2, verbose=False)
            if os.path.isdir(proc_dir3):
                imsave(img_out_path, b_array)

    # DILATE
    if False:
        for img_file in find_image_files(proc_dir3):
            img_basename = os.path.basename(img_file)
            img_out_path = os.path.join(proc_dir4, img_basename)

            im_array = imopen(img_file)
            b_array = binary_dilation(
                im_array.astype('bool'), iterations=3)
            if os.path.isdir(proc_dir4):
                imsave(img_out_path, b_array)

    # MASK:
    if False:
        for img_file in find_image_files(proc_dir4):
            img_basename = os.path.basename(img_file)
            img_out_path = os.path.join(proc_dir5, img_basename)
            base_file = os.path.join(base_dir, img_basename)

            if os.path.isfile(base_file):
                base_array = imopen(base_file)
                mask_array = imopen(img_file)
                b_array = mask(base_array, mask_array)
                if os.path.isdir(proc_dir5):
                    imsave(img_out_path, b_array)
