#!/usr/bin/python
#
# find_neighbors.py
#
# LAST EDIT: 2017-06-21
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# REQUIRED MODULES:
###############################################################################
import numpy

from .imutil import is_binary


__all__ = ['ELEMENT', 'find_nearest', 'find_neighbors', 'find_objects',
           'get_particle_sizes']


###############################################################################
# CLASS:
###############################################################################
class ELEMENT():
    """
    Name:     ELEMENT
    Features: Class representing a discrete, continuous body of foreground
              pixels, called an element.
    """
    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Initialization
    # ////////////////////////////////////////////////////////////////////////
    def __init__(self, data):
        """
        Name:     element.__init__
        Inputs:   set(tuple(int)), set of coordinate points (data)
        Outputs:  None.
        Features: Class initialization
        """
        self._orig = data       # coordinate data points
        self._size = None       # total number of pixels in element

    def __str__(self):
        """
        Name:     element.__str__
        Inputs:   None.
        Outputs:  str
        Features: Class string property, accessed via the print function
        """
        pstr = '{} - \n'.format(self.__class__)
        pstr += 'element size: {:d}[px]\n'.format(self.size)
        return pstr

    # \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    # Class Property Definitions
    # ////////////////////////////////////////////////////////////////////////
    @property
    def points(self):
        """
        The list of original coordinate points
        """
        return list(self._orig)

    @points.setter
    def points(self, val):
        raise AttributeError('manual setting of size is forbidden')

    @property
    def size(self):
        """
        The total number of pixels in the element
        """
        if self._size is not None:
            return self._size
        else:
            self._size = len(self._orig)
            return self._size

    @size.setter
    def size(self, val):
        raise AttributeError('manual setting of size is forbidden')


###############################################################################
# FUNCTIONS:
###############################################################################
def find_neighbors(my_array, coord):
    """
    Name:     find_neighbors
    Inputs:   - numpy.ndarray, binary image array (my_array)
              - tuple(int, int), starting location coordinates (coord)
    Outputs:  set, coordinates of connected pixels with given location
    Features: Finds all black pixels neighboring the given pixel
    Depends:  find_nearest
    """
    visited = set()
    to_visit = set()
    visited.add(coord)
    to_visit.update(find_nearest(my_array, coord, visited))

    # loop until all neighbors to visit have been visited
    while len(to_visit.difference(visited)) != 0:
        # accumulate new neighbors
        new_neighbors = set()
        for loc in to_visit:
            # update visited list and add its new neighbors
            visited.add(loc)
            new_neighbors.update(find_nearest(my_array, loc, visited))
        # remove visited pixels from new to visit list
        to_visit = new_neighbors

    return visited


def find_objects(bin_array):
    """
    Name:     find_objects
    Inputs:   numpy.ndarray, a binary image array (bin_array)
    Outputs:  list, ELEMENT list
    Features: finds all continuous groups of black pixels that cross the
              horizontal center of the image and displays them as a new image
    Depends:  - find_neighbors
              - is_binary
    """
    if not is_binary(bin_array):
        raise TypeError("Image array must be binary!")
    else:
        bin_array = bin_array.astype("bool")

    # Instantiate sets:
    # NOTE: visited is the set of all coords whose neighbors have been found
    elements = []
    visited = set()
    fg_pixels = numpy.where(bin_array)
    yids, xids = fg_pixels
    for i in range(len(xids)):
        n = xids[i]
        m = yids[i]
        if (m, n) not in visited:
            # New pixel found! Perform next search:
            elem = ELEMENT(data=find_neighbors(bin_array, (m, n)))
            elements.append(elem)
            visited.update(elem._orig)
    return elements


def find_nearest(my_array, coord, visited):
    """
    Name:     find_nearest
    Inputs:   - numpy.ndarray, a binary image array (my_array)
              - tuple, starting location coordinates (coord)
              - set, coordinates already visited (visited)
    Outputs:  set, unvisted neighboring foreground pixel coords (neighbors)
    Features: Returns neighboring foreground pixel coordinates that have yet
              to be visited
    """
    y, x = coord
    h, w = my_array.shape
    neighbors = []

    # determine eight-point neighbor coordinates
    cords = [(y, x-1), (y-1, x), (y+1, x), (y, x+1),
             (y-1, x-1), (y-1, x+1), (y+1, x-1), (y+1, x+1)]

    # For each location, check if it's in the image, foreground and unvisted
    for loc in cords:
        if (loc[0] >= 0 and loc[0] < h):
            if (loc[1] >= 0 and loc[1] < w):
                if my_array[loc]:
                    if loc not in visited:
                        neighbors.append(loc)
    return set(neighbors)


def get_particle_sizes(my_objects):
    """
    Name:     get_particle_sizes
    Inputs:   list, ELEMENT object list
    Outputs:  numpy.ndarray, sorted sizes
    Features: Returns an array of ELEMENT sizes
    """
    sizes = []
    for p in my_objects:
        sizes.append(p.size)
    sizes = sorted(sizes)
    sizes = numpy.array(sizes)
    return sizes
