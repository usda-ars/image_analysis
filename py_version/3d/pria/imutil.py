#!/usr/bin/python
#
# imutil.py
#
# LAST EDIT: 2017-06-21
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
###############################################################################
# IMPORT MODULES:
###############################################################################
import os

import numpy
from PIL import Image
try:
    import rawpy
    raw_support = True
except ImportError:
    raw_support = False
from scipy.misc import imread
from scipy.misc import imsave

from .utilities import print_progress


__all__ = ['imopen',
           'imsave',
           'is_binary',
           'is_rgb',
           'otsu',
           'rgb_to_b',
           'rgb_to_cmyk',
           'rgb_to_g',
           'rgb_to_gray',
           'rgb_to_max',
           'rgb_to_min',
           'rgb_to_r',
           'rgb_to_y',
           'sauvola',
           'show_image']


###############################################################################
# FUNCTIONS
###############################################################################
def calc_statistics(my_array):
    """
    Name:     calc_statistics
    Inputs:   numpy.ndarray (my_array)
    Outputs:  dict, statistical quantities
              - float, max value (max)
              - float, min value (min)
              - float, mean value (ave)
              - float, standard deviation (std)
              - float, skewness (skew)
              - float, kurtosis (kurt)
    Features: Returns the basic/advanced statistics for an array of values
    """
    # Initialize return dictionary:
    stat_dict = {"max": None, "min": None, "ave": None,
                 "std": None, "skew": None, "kurt": None}

    # Make sure my_array is a numpy array:
    if not isinstance(my_array, numpy.ndarray):
        my_array = numpy.array(my_array)

    # Make sure my_array is not empty or crashes on skew/kurt:
    num_val = len(my_array)
    if my_array.any() and num_val > 1:
        # Max, min, mean, st dev, skew, kurtosis (offset from normal)
        stat_dict['max'] = my_array.max()
        stat_dict['min'] = my_array.min()
        ave_val = my_array.mean()
        stat_dict['ave'] = ave_val
        std_val = my_array.std()
        stat_dict['std'] = std_val

        # Address divide by zero issues:
        if std_val == 0:
            std_val = 1e-4

        stat_dict['skew'] = (
            sum((my_array - ave_val)**3)/((num_val)*std_val**3))
        stat_dict['kurt'] = (
            sum((my_array - ave_val)**4)/((num_val)*std_val**4) - 3)
    else:
        print("Stats warning: insufficient data for stats calculation")

    return stat_dict


def imopen(img_path):
    """
    Name:     imopen
    Inputs:   str, path to image file (img_path)
    Outputs:  numpy.ndarray, image array
    Features: Extends the PIL util imread for raw image types
    Depends:  imread
    """
    if not os.path.isfile(img_path):
        err_msg = "'%s' does not exist!" % (img_path)
        raise IOError(err_msg)

    img_name = os.path.basename(img_path)
    img_base, img_ext = os.path.splitext(img_name)

    if img_ext in ['.NEF', '.nef'] and raw_support:
        # Use libraw image reader:
        raw = rawpy.imread(img_path)
        img = raw.postprocess()
    else:
        # Use scipy PIL image reader:
        img = imread(img_path)

    return img


def is_binary(nd_array):
    """
    Name:     is_binary
    Inputs:   numpy.ndarray, image array (nd_array)
    Outputs:  bool
    Features: Returns whether an image array is binary
    """
    # Check #1: is array:
    if not isinstance(nd_array, numpy.ndarray):
        return False

    # Check #2: is 2D array:
    img_dim = len(nd_array.shape)
    if img_dim != 2:
        return False

    # Check #3: pixel colors:
    gray_pixels = numpy.where(
        (nd_array > nd_array.min()) & (nd_array < nd_array.max()))
    gray_idx, gray_idy = gray_pixels
    if len(gray_idx) == 0 and len(gray_idy) == 0:
        # All pixels are either max or min value
        return True
    else:
        return False


def is_rgb(nd_array):
    """
    Name:     is_rgb
    Inputs:   numpy.ndarray, image array (nd_array)
    Outputs:  bool
    Features: Returns whether an image array is RGB
    """
    # Check 1: is array:
    if not isinstance(nd_array, numpy.ndarray):
        return False

    # Checks 2/3: is 3D array with 3 color bands:
    img_dim = len(nd_array.shape)
    if img_dim == 3 and nd_array.shape[2] == 3:
        # Check array values are numeric (fails on strings, decimals, etc.)
        if numpy.str(nd_array[0, 0, 0]).isnumeric:
            return True
        else:
            return False
    else:
        return False


def otsu(nd_array, verbose=False):
    """
    Name:     otsu
    Inputs:   - numpy.ndarray, 2D integer array (nd_array)
              - [optional] bool, verbose
    Outputs:  int, Otsu's threshold value
    Features: Returns the thresholding pixel value based on the Otsu (1979)
              method.
    Ref:      Otsu, N. (1979) A threshold selection method from gray-level
              histograms, IEEE Transactions on Systems, Man, and
              Cybernetics, 9(1), 62--66.
    """
    # Otsu's number of pixels at each level, n, total number of pixels, N,
    # and normalized probability distribution, p
    n = numpy.bincount(nd_array.astype("uint8").ravel(), minlength=256)
    N = n.sum()
    p = n.astype("f4") / N

    # Otsu's mean pixel value at each level, mu:
    mu = [i * p[i] for i in range(len(p))]
    mu = numpy.array(mu)

    # Otsu's probabilities of class occurrence, wk [Eq. 6], class mean
    # values, uk [Eq. 7], and the total mean level of the original
    # image, ut [Eq. 8]:
    wk = p.cumsum()
    uk = mu.cumsum()
    ut = mu.sum()

    # Calculate the between-class variances, vb [Eq. 18]:
    vb = ut * wk[:-1]
    vb -= uk[:-1]
    vb = numpy.power(vb, 2.0)
    denom = 1. - wk[:-1]
    denom *= wk[:-1]
    if 0 in denom:
        if verbose:
            print("Otsu warning: zeros in denominator, adjusting")
        denom[numpy.where(denom == 0)] += 1.e-3
    try:
        vb /= denom
    except:
        raise
    else:
        # Search for maximum between-class variance, i.e., threshold value:
        k = numpy.where(vb == vb.max())[0]
        if len(k) == 0:
            if verbose:
                print("Otsu error: failed to find maximum value")
            raise ValueError("No maximum value found in Otsu method")
        elif len(k) > 1:
            if verbose:
                print(("Otsu warning: found %d maximum values, "
                       "using first occurrence") % len(k))
        thresh = k[0]
        if verbose:
            print("Otsu: threshold set at %s" % thresh)

        return thresh


def rgb_to_b(nd_array):
    """
    Name:     rgb_to_b
    Inputs:   numpy.ndarray, RGB color image array (nd_array)
    Outputs:  numpy.ndarray, 2D grayscale image array of blue color space
    Features: Converts color image to grayscale based on the blue colorband
    Depends:  is_rgb
    """
    if is_rgb(nd_array):
        B = nd_array[:, :, 2]
        return B
    else:
        raise TypeError("Image array is not RGB!")


def rgb_to_cmyk(r, g, b):
    """
    Name:     rgb_to_cmyk
    Inputs:   - int, red pixel value (r)
              - int, green pixel value (g)
              - int, blue pixel value (b)
    Outputs:  tuple, CMYK floats
    Features: Converts red green blue (RGB) pixel values (0--255) to
              cyan magenta yellow black (CMYK) color space values (0--1)
    Ref:      Adapted from "RGB-to-CMYK Color Conversion" Online:
              http://www.javascripter.net/faq/rgb2cmyk.htm
    """
    if r == 0 and g == 0 and b == 0:
        return (0, 0, 0, 1)
    elif r < 0 or g < 0 or b < 0 or r > 255 or g > 255 or b > 255:
        raise ValueError("RGB values must be between 0 and 255")
    else:
        computedC = 1.0 - (r/255.0)
        computedM = 1.0 - (g/255.0)
        computedY = 1.0 - (b/255.0)

        minCMY = min([computedC, computedM, computedY])

        computedC = (computedC - minCMY) / (1.0 - minCMY)
        computedM = (computedM - minCMY) / (1.0 - minCMY)
        computedY = (computedY - minCMY) / (1.0 - minCMY)
        computedK = minCMY

        return (computedC, computedM, computedY, computedK)


def rgb_to_g(nd_array):
    """
    Name:     rgb_to_g
    Inputs:   numpy.ndarray, RGB color image array (nd_array)
    Outputs:  numpy.ndarray, 2D grayscale image array of green color space
    Features: Converts color image to grayscale based on the green colorband
    Depends:  is_rgb
    """
    if is_rgb(nd_array):
        G = nd_array[:, :, 1]
        return G
    else:
        raise TypeError("Image array is not RGB!")


def rgb_to_gray(nd_array):
    """
    Name:     rgb_to_gray
    Inputs:   numpy.ndarray, RGB color image array (nd_array)
    Outputs:  numpy.ndarray, 2D grayscale image array
    Features: Converts color image to grayscale based on a weighted sum
              of the color bands as a calculation of luminance
    Depends:  is_rgb
    """
    if is_rgb(nd_array):
        R = nd_array[:, :, 0]
        G = nd_array[:, :, 1]
        B = nd_array[:, :, 2]
        Y = (0.299 * R) + (0.587 * G) + (0.114 * B)
        return Y
    else:
        img_dim = len(nd_array.shape)
        if img_dim == 2:
            print("Already grayscale.")
            return nd_array
        else:
            raise TypeError("Image array is not RGB!")


def rgb_to_max(nd_array):
    """
    Name:     rgb_to_max
    Inputs:   numpy.ndarray, RGB color image array (nd_array)
    Outputs:  numpy.ndarray, 2D grayscale image array
    Features: Converts color image to grayscale based on the maximum pixel
              value of the color bands
    Depends:  is_rgb
    """
    if is_rgb(nd_array):
        max_array = numpy.max(nd_array, 2)
        return max_array
    else:
        # image is already grayscale, max is equivalent to original
        return nd_array


def rgb_to_min(nd_array):
    """
    Name:     rgb_to_min
    Inputs:   numpy.ndarray, RGB color image array (nd_array)
    Outputs:  numpy.ndarray, 2D grayscale image array
    Features: Converts color image to grayscale based on the minimum pixel
              value of the color bands
    Depends:  is_rgb
    """
    if is_rgb(nd_array):
        min_array = numpy.min(nd_array, 2)
        return min_array
    else:
        # image is already grayscale, max is equivalent to original
        return nd_array


def rgb_to_r(nd_array):
    """
    Name:     rgb_to_r
    Inputs:   numpy.ndarray, RGB color image array (nd_array)
    Outputs:  numpy.ndarray, 2D grayscale image array of red color space
    Features: Converts color image to grayscale based on the red colorband
    Depends:  is_rgb
    """
    if is_rgb(nd_array):
        R = nd_array[:, :, 0]
        return R
    else:
        raise TypeError("Image array is not RGB!")


def rgb_to_y(nd_array):
    """
    Name:     rgb_to_y
    Inputs:   numpy.ndarray, RGB image array (nd_array)
    Outputs:  numpy.ndarray, yellow image array
    Features: Converts RGB image to yellow color space values (0--1)
    Ref:      Adapted from "RGB-to-CMYK Color Conversion" Online:
              http://www.javascripter.net/faq/rgb2cmyk.htm
    """
    if is_rgb(nd_array):
        r = nd_array[:, :, 0]
        g = nd_array[:, :, 1]
        b = nd_array[:, :, 2]

        computedC = numpy.ones(r.shape)
        computedC -= r/255.0

        computedM = numpy.ones(g.shape)
        computedM -= g/255.0

        computedY = numpy.ones(b.shape)
        computedY -= b/255.0

        minCMY = numpy.zeros(r.shape)
        numpy.minimum(computedC, computedM, out=minCMY)
        numpy.minimum(computedY, minCMY, out=minCMY)

        computedY -= minCMY

        minCMY *= -1
        minCMY += 1

        nodividx = numpy.where(minCMY == 0)
        computedY[nodividx] *= 0

        divokay = numpy.where(minCMY != 0)
        computedY[divokay] = computedY[divokay] / minCMY[divokay]

        return computedY

    else:
        raise TypeError("Input array is not RGB!")


def sauvola(nd_array, d, verbose=False):
    """
    Name:     sauvola
    Inputs:   - numpy.ndarray, grayscale image (nd_array)
              - int, half-kernel width (d)
              - [optional] bool, verbose output (verbose)
    Features: Returns a binary image based on the local adaptive thresholding
              technique described in Sauvola and Pietikainen (2000)
    Depends:  print_progress
    Ref:      Sauvola and Pietikainen (2000), Adaptive document image
              binarization, Pattern Recognition, 33(2), 225--236.
    Note:     very slow
    """
    k = 0.5
    R = 128.0
    if not is_rgb(nd_array):
        b_array = numpy.zeros(nd_array.shape, dtype='bool')
        h, w = nd_array.shape
        if verbose:
            print_progress(0, h - d - 1, "Threshed")
        for i in range(d, h - d - 1):
            for j in range(d, w - d - 1):
                temp_array = nd_array[i-d:i+d+1, j-d:j+d+1]
                mij = temp_array.mean()
                sij = temp_array.std()
                tij = mij*(1.0 + k*(sij/R - 1.0))
                # Use > rather than >= to avoid whitening
                # the swaths of black where T == 0
                if nd_array[i, j] > tij:
                    b_array[i, j] = True
            if verbose:
                print_progress(i+1, h - d - 1, "Threshed")
        return b_array
    else:
        raise TypeError("Input array is RGB!")


def show_image(nd_array, auto_rotate=False):
    """
    Name:     show_image
    Input:    - numpy.ndarray, image data (nd_array)
              - [optional] bool, whether to auto rotate (auto_rotate)
    Output:   None.
    Features: Shows numpy image array
    """
    # Binary image support:
    if nd_array.dtype == "bool":
        nd_array = nd_array.astype("uint8")

    img_dim = len(nd_array.shape)
    if nd_array.max() == 1 and img_dim == 2:
        nd_array[numpy.where(nd_array == 1)] *= 255

    try:
        img = Image.fromarray(nd_array)
    except:
        raise
    else:
        if auto_rotate and img.size[0] > img.size[1]:
            img = img.transpose(Image.ROTATE_90)
        img.show()
