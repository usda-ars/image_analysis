#!/usr/bin/python
#
# setup.py
#
# LAST EDIT: 2017-06-21
#
###############################################################################
# PUBLIC DOMAIN NOTICE                                                        #
###############################################################################
# This software is a "United States Government Work" under the terms of the   #
# United States Copyright Act.  It was written as part of the authors'        #
# official duties as a United States Government employee and thus cannot be   #
# copyrighted.  This software/database is freely available to the public for  #
# use. The Department of Agriculture (USDA) and the U.S. Government have not  #
# placed any restriction on its use or reproduction.                          #
#                                                                             #
# Although all reasonable efforts have been taken to ensure the accuracy and  #
# reliability of the software, the USDA and the U.S. Government do not and    #
# cannot warrant the performance or results that may be obtained by using     #
# this software. The USDA and the U.S. Government disclaim all warranties,    #
# express or implied, including warranties of performance, merchantability or #
# fitness for any particular purpose.                                         #
#                                                                             #
# Please cite the authors in any work or product based on this material.      #
#     Tyler Davis, Dave Schneider, Hoi Cheng, Nathanael Shaw                  #
#     Robert W. Holley Center for Agriculture and Health                      #
#     USDA-Agricultural Research Service                                      #
#     538 Tower Road, Ithaca NY, 14853                                        #
###############################################################################
#
#
# ------------
# description:
# ------------
# This is a command-line program for image processing digital images of root
# system architecture (RSA) and is a part of the RSA imaging, processing and
# analysis project.
#
# ----------
# changelog:
# ----------
# - created [15.05.22]
# - added OpenCV (cv2) and SCIKITS-IMAGE packages [15.05.26]
#   --> included numpy and matplotlib for arrays and plotting
# - added pyqtgraph package [15.06.03]
#   --> installed PyQt 4.11 (Canopy package manager) and pyqtgraph (via pip)
# - added scipy.ndimage package [17.03.31]
# - created new workflow [17.03.31]
# - added manager class and made this a command line tool [17.04.03]
# - added python checks before execution [17.04.03]
# - updated check functions [17.04.05]
# - moved print messages to utilities [17.04.06]
# - moved manager import to main to catch import errors [17.04.06]
# - renamed prip to setup [17.04.10]
# - created pipr "Prida Image Processing for RootReader3D" package [17.04.10]
# - removed pilutil; rather, import from scipy.misc [17.04.10]
# - changed pipr back to prip [17.04.13]
# - changed prip to pria [17.04.26]
# - made rawpy an optional dependency [17.06.21]
#
###############################################################################
# IMPORT MODULES:
###############################################################################
import sys

from pria.utilities import print_messages


###############################################################################
# FUNCTIONS:
###############################################################################
def check_numpy(tot=49):
    """
    Name:     check_numpy
    Inputs:   [optional] int, total line character limit (tot)
    Outputs:  - bool, (passed)
              - str, error message (err)
    Features: Checks if numpy package is available
    """
    passed = False
    err = None
    a = "Checking for numpy"  # 18 chars
    try:
        import numpy
    except ImportError:
        err = ("Python package numpy is not installed! "
               "Please install numpy (e.g., pip install numpy).")
        print("{} {} FAILED".format(a, "."*(tot - len(a))))
    else:
        passed = True
        b = "(found v.{})".format(numpy.__version__)
        print("{} {} {} OK".format(a, b, "."*(tot - len(a) - len(b) - 1)))

    return (passed, err)


def check_pil(tot=49):
    """
    Name:     check_pil
    Inputs:   [optional] int, total line character limit (tot)
    Outputs:  - bool, (passed)
              - str, error message (err)
    Features: Checks if PIL package is available
    """
    passed = False
    err = None
    a = "Checking for PIL"
    try:
        import PIL.Image
    except ImportError:
        err = ("Python package PIL is not installed! "
               "Please install PIL (e.g., pip install Pillow).")
        print("{} {} FAILED".format(a, "."*(tot - len(a))))
    else:
        passed = True
        b = "(found v.{})".format(PIL.Image.VERSION)
        print("{} {} {} OK ".format(a, b, "."*(tot - len(a) - len(b) - 1)))

    return (passed, err)


def check_pria(tot=49):
    """
    Name:     check_pria
    Inputs:   [optional] int, total line character limit (tot)
    Outputs:  - bool, (passed)
              - str, error message (err)
    Features: Checks the pria package version
    """
    passed = False
    err = None
    a = "Checking for pria"
    try:
        import pria
    except ImportError:
        err = ("Python package pria is missing! "
               "Please check that this script is in the correct directory.")
        print("{} {} FAILED".format(a, "."*(tot - len(a))))
    else:
        passed = True
        b = "(found v.{})".format(pria.__version__)
        print("{} {} {} OK ".format(a, b, "."*(tot - len(a) - len(b) - 1)))

    return (passed, err)


def check_python3(tot=49):
    """
    Name:     check_python3
    Inputs:   [optional] int, total line character limit (tot)
    Outputs:  - bool, (passed)
              - str, error message (err)
    Features: Checks if the main version of Python is version 3
    """
    passed = False
    err = None
    a = "Checking for Python 3"
    if sys.version_info[0] == 3:
        passed = True
        b = "(running v.{}.{}.{})".format(
            sys.version_info[0], sys.version_info[1], sys.version_info[2])
        print("{} {} {} OK".format(a, b, "."*(tot - len(a) - len(b) - 1)))
    else:
        err = ("Python 3 not running! "
               "Python 3 is required for running this software.")
        print("{} {} FAILED".format(a, "."*(tot - len(a))))

    return (passed, err)


def check_rawpy(tot=49):
    """
    Name:     check_rawpy
    Inputs:   [optional] int, total line character limit (tot)
    Outputs:  - bool, (passed)
              - str, error message (err)
    Features: Checks if the rawpy package is available
    """
    passed = False
    err = None
    a = "Checking for rawpy"
    try:
        import rawpy
    except ImportError:
        err = ("Python package rawpy is unavailable. "
               "For raw image support (e.g., .NEF), "
               "please install rawpy (e.g., pip install rawpy).")
        print("{} {} N/A".format(a, "."*(tot - len(a))))
    else:
        passed = True
        b = "(found v.{})".format(rawpy.__version__)
        print("{} {} {} OK".format(a, b, "."*(tot - len(a) - len(b) - 1)))

    return (passed, err)


def check_scipy(tot=49):
    """
    Name:     check_scipy
    Inputs:   [optional] int, total line character limit (tot)
    Outputs:  - bool, (passed)
              - str, error message (err)
    Features: Checks if the scipy package is available
    """
    passed = False
    err = None
    a = "Checking for scipy"  # 18 chars
    try:
        import scipy
        import scipy.misc
        import scipy.optimize
    except ImportError:
        err = ("Python package scipy is not installed! "
               "This package is required for analysis mode. "
               "For analysis mode, please install scipy "
               "(e.g., pip install scipy).")
        print("{} {} FAILED".format(a, "."*(tot - len(a))))
    else:
        passed = True
        b = "(found v.{})".format(scipy.__version__)
        print("{} {} {} OK".format(a, b, "."*(tot - len(a) - len(b) - 1)))

    return (passed, err)


def perform_checks(fline=49):
    """
    Name:     perform_checks
    Inputs:   [optional] int, character fill line length (fline)
    Outputs:  - bool, okay to procees flag
              - list, checks passed versus checks performed
              - list, error messages
    Features: Runs a series of checks for Prida software development
    """
    passed = [0, 0]
    to_proceed = True
    err_msgs = []

    # CHECK: Python 3 requirement
    c_py3, e_py3 = check_python3(fline)
    passed[1] += 1
    if c_py3:
        passed[0] += 1
    else:
        to_proceed = False
    if e_py3 is not None:
        err_msgs.append(e_py3)

    # CHECK: pria package
    c_pria, e_pria = check_pria(fline)
    passed[1] += 1
    if c_pria:
        passed[0] += 1
    else:
        to_proceed = False
    if e_pria is not None:
        err_msgs.append(e_pria)

    # CHECK: Python numpy
    c_numpy, e_numpy = check_numpy(fline)
    passed[1] += 1
    if c_numpy:
        passed[0] += 1
    else:
        to_proceed = False
    if e_numpy is not None:
        err_msgs.append(e_numpy)

    # CHECK: Python scipy
    c_scipy, e_scipy = check_scipy(fline)
    passed[1] += 1
    if c_scipy:
        passed[0] += 1
    else:
        to_proceed = False
    if e_numpy is not None:
        err_msgs.append(e_scipy)

    # CHECK: Python Image Library (PIL)
    c_pil, e_pil = check_pil(fline)
    passed[1] += 1
    if c_pil:
        passed[0] += 1
    else:
        to_proceed = False
    if e_pil is not None:
        err_msgs.append(e_pil)

    # CHECK: Python rawpy package (optional)
    c_rawpy, e_rawpy = check_rawpy(fline)
    passed[1] += 1
    if c_rawpy:
        passed[0] += 1
    if e_rawpy is not None:
        err_msgs.append(e_rawpy)

    return (to_proceed, passed, err_msgs)


###############################################################################
# MAIN PROGRAM
###############################################################################
if __name__ == '__main__':
    line_len = 57
    greeting = " 3D Image Analysis Setup "
    ending = " end setup "
    gdots = int(0.5*(line_len - len(greeting)))
    edots = int(0.5*(line_len - len(ending)))
    print("{}{}{}".format("-"*gdots, greeting, "-"*gdots))

    # Perform system checks:
    to_proceed, num_passed, messages = perform_checks()

    if to_proceed:
        print("{}{}{}".format('-'*edots, ending, '-'*edots))
        print("Passed {}/{}".format(num_passed[0], num_passed[1]))
        print("")

        if num_passed[0] < num_passed[1]:
            print("Encountered the following warnings:")
            print_messages(messages, line_len)
            print("")

        # Run the manager:
        from pria.manager import MANAGER
        my_manager = MANAGER(sys.argv)
        my_manager.run()
    else:
        print("{}{}{}".format('-'*edots, ending, '-'*edots))
        print("Passed {}/{}".format(num_passed[0], num_passed[1]))
        print("")
        print("Encountered the following errors:")
        print_messages(messages, line_len)
