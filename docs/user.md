# Python Version
The `pria` package is a command-line tool for processing digital images of root systems, which has been successfully compiled and tested with Python 3[^1] (version 3.5) and depends on the third-party packages NumPy[^2] (version 1.11.3), SciPy[^3] (version 0.18.3), Pillow[^4] (version 3.4.2), and RawPy[^5] (version 0.7.0; optional for NEF[^6] file support&mdash;depends on the libraw[^7] library).

## Install
Python 3 is a high-level computer-programming language and runtime environment.
Python is available for all major operating systems and can be downloaded free of charge from their website http://www.python.org/.

To check that Python was installed correctly, open a command prompt (or terminal window) and type `python -V` and hit return.
You should see something like the following.

```
$ python -V
Python 3.5.2
```

If you received a message like:

```bash
$ python -V
'python' is not recognized as an internal or external command, operable program or batch file
```

then try typing `python3 -V`.
If that also does not work, then Python was not added to your computer's environment path.
To fix this issue, try reinstalling Python and selecting Advanced Installation Options, and making certain to check the box next to "Add Python to environment path."

If the version of Python shown is inconsistent with the version you just installed (e.g., it reads "Python 2.7.5" but you installed Python 3.5.2), then try typing `python3 -V`.

!!! note "Windows Users"
    Python provides an optional `py` tool that may be selected during installation. This tool can be used in place of `python` from the command prompt.

Pip is a Python package manager and is the recommended method for installing new Python packages to your computer.
Most of the latest Python installations now include pip by default.

To check if you have pip installed, type `pip --version` in the command prompt and hit enter.
You should see something similar to the following:

```bash
$ pip --version
pip 9.0.1 from c:\program files\python35\lib\site-packages
```

Install the following Python packages using pip:

- numpy (e.g., `pip install numpy`)
- scipy (e.g., `pip install scipy`)
- Pillow (e.g., `pip install Pillow`)
- rawpy (e.g., `pip install rawpy`)

If you are not using a virtual environment, you may have to include the `--user` flag to install locally (e.g., `pip install --user rawpy`) or run the command as an administrator (e.g., opening a Command Prompt as Administrator or prepending the command with `sudo` on Unix-like systems).

!!! attention "Python Libraries for Windows"
    If you are running Windows, do not use the default `pip install` for numpy, scipy and rawpy due to missing dependencies. Instead, download the Windows wheel (.whl) files (binaries used by pip) from the following sites. Make certain you match the wheel file with the version of Python (e.g., 'cp35' for Python 3.5) and OS (e.g., win32 for Window 32-bit) you have installed based on the filename.

    - [numpy](http://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy)
    - [scipy](http://www.lfd.uci.edu/~gohlke/pythonlibs/#scipy)
    - [rawpy](https://pypi.python.org/pypi/rawpy)

    To install these wheel files with pip, open a Command Prompt (as Administrator if installing to system directory), navigate to the folder where you downloaded the wheel files and run the pip install command. For example, use:

    `pip install numpy‑1.11.3+mkl‑cp35‑cp35m‑win32.whl`

    to install the numpy version 1.11.3 for Python 3.5 on Windows 32-bit wheel.


To acquire a copy of the `pria` package, you may either download or clone the public Bitbucket repository at [bitbucket.org/usda-ars/image_analysis](https://bitbucket.org/usda-ars/image_analysis).
To clone the repository, you will need to install Git, which is freely available for all major operating systems.

!!! attention "Git for Windows"
    To clone the Image Analysis repository, you will need to install Git, which may be freely downloaded from [https://git-scm.com/download/win](https://git-scm.com/downloads).

To create a repository clone, open a Command Prompt or Terminal and run the following command:

```bash
git clone https://bitbucket.org/usda-ars/image_analysis.git
```

else, download a compressed copy of the repository from the Bitbucket repository's Download page and unzip it.

This will create a local copy of the `image_analysis/` directory with the following folder hierarchy (updated for version 0.8):

```plain
./
| - image_analysis/
    | - mkdocs.yml
    | - docs/
    | - py_version/
        | - 3d/
            | - setup.py
            | - pria/
                | - __init__.py
                | - dustutil.py
                | - imutil.py
                | - manager.py
                | - processes.py
                | - utilities.py
```

The `pria` Python package and setup script are located in the `image_analysis\py_version\3d` directory.

## Setup
The `pria` package comes with a `setup.py` script that may be used to launch the command-line tool.

!!! note
    This setup script does not install the PRIA package to your local Python site-packages

To begin the command-line tool, open a Terminal, navigate to the folder where the `setup.py` script is located and run the following command:

```bash
$ python setup.py
---------------- 3D Image Analysis Setup ----------------
Checking for Python 3 (running v.3.6.1) .......... OK
Checking for pria (found v.0.9.0) ................ OK
Checking for numpy (found v.1.13.0) .............. OK
Checking for scipy (found v.0.19.0) .............. OK
Checking for PIL (found v.1.1.7) ................. OK
Checking for rawpy ............................... N/A
----------------------- end setup -----------------------
Passed 5/6

Encountered the following warnings:
 1. Python package rawpy is unavailable. For raw image
    support (e.g., .NEF), please install rawpy (e.g., pip
    install rawpy).
---------------------------------------------------------
```

The setup script checks your Python environment to make certain the required dependencies, listed above, are met.
The setup script terminates and prints error messages for all unmet dependencies; otherwise, the command-line tool launches.

The `setup.py` script allows for optional command-line arguments to be passed for assigning certain processing variables.
The following table lists the command-line arguments that are currently supported, the parameter that they attribute to, and possible assignment values.

| Key  | Value | Description |
| :--: | :---- | :---------- |
| -d   | (path) | Path to working directory where your image files for processing are stored |
| -c   | (black/white) | Dominant image background color (used in merging and thresholding) |
| -g   | (number) | Standard deviation for the Gaussian blur used in the color mask; set value to 'None' to skip this processing step |
| -i   | (integer) | Number of binary dilation iterations

[^1]: Copyright &copy; 2001&ndash;2017 Python Software Foundation. [www.python.org](https://www.python.org)
[^2]: Copyright &copy; 2005&ndash;2016 NumPy Developers. [www.numpy.org](http://www.numpy.org)
[^3]: Copyright &copy; 2003&ndash;2016 SciPy Developers. [www.scipy.org](https://www.scipy.org)
[^4]: Copyright &copy; 1995&ndash;2011. Fredrik Lundh, 2010&ndash;2017 Alex Clark and Contributors. [https://python-pillow.org](https://python-pillow.org)
[^5]: Copyright &copy; 2014 Mark Riechart. [https://github.com/letmaik/rawpy](https://github.com/letmaik/rawpy)
[^6]: Nikon Electronic Format (NEF). Copyright &copy; 2017 Nikon Inc.
[^7]: Copyright &copy; 2008&ndash;2016 Libraw LLC; includes source code from dcraw.c by Dave Coffin &copy; 1997&ndash;2016.
