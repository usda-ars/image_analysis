# Color Mask
The first technique to partition roots imaged in the 3D system is by applying a color mask over the original color image before converting it to binary.
The goal of this color mask is to avoid regions of yellow within the image, which consist mainly of the fishing line mesh.

### Overview
1. Extract blue color band
1. Scale/blur image
1. Stretch pixel values
1. Binarize
1. Blackout border
1. Remove dust
1. Dilate

### Blue extraction and blurring
The first processing step is to extract the blue color band from the original red-green-blue (RGB) color image.
Since blue is the complementary color of yellow, the yellow fishing line is significantly muted in the blue color band.
Acute imaging angles (i.e., the mesh appears more solid) and lighting irregularities can cause bright zones where the mesh is still visible in the blue color band.

Pixels may be blurred to further eliminate the mesh from the blue color band, either through image scaling (i.e., reducing the resolution) or by employing a Gaussian filter.
In both cases, mesh is removed from the image at the cost of losing fine details from the roots.

<div>
<img src="../images/example_img.jpg" alt="Original color image" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<img src="../images/cmask_blue.jpg" alt="Color mask blue channel" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<img src="../images/cmask_gauss.jpg" alt="Color mask Gaussian blur" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<p style="clear: both;">
Figure 1. (left to right) Original color image, the blue color channel, and the blue channel following a Gaussian blur with standard deviation = 2.0
</div>

### Stretch
Stretching the pixel brightness in the blue channel may be used to improve contrast within the images.
Brightness stretching first examines the maximum and minimum pixel values within an image and then performs a linear scaling such that the range of pixel brightness spans values from 0 to 255.
The stretching equation is given as the following:

$$s(p_i) = 255\frac{p_i - \min(p)}{\max(p) - \min(p)}$$

where *s* is the stretched brightness value for the *i*-th pixel, *p*, in the image.

### Binarize, border and filter
The blue-channel grayscale image is converted to a binary image based on the Otsu method ([Otsu, 1979](references)).

A border may be prescribed to manually black out regions surrounding the region of interest.
The border is defined by the relative extents of the region of interest, which by default spans from 0-100% from top to bottom and left to right.
Extents are relative to the top-left corner (0%, 0%).

A dust-removal algorithm may be employed to further remove specks or unwanted noise from the region of interest.
The binarized and bordered image is scanned for white pixels.
White pixels are grouped into clusters based on a nearest-neighbor search algorithm.
Pixel clusters that are smaller than the prescribed size are removed from the binary image.

<div>
<img src="../images/cmask_bin.jpg" alt="Color mask binary" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<img src="../images/cmask_border.jpg" alt="Color mask border" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<img src="../images/cmask_dust.jpg" alt="Color mask dust-filtered" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<p style="clear: both;">
Figure 3. (left to right) Binary image, binary image with a blackout border, and dust-filtered binary image for clusters less than or equal to 25 pixels
</div>

### Dilation
The mask may be expanded by applying a number of dilation iterations.
Each iteration grows the mask by a single pixel width.

<div>
<img src="../images/cmask_dust.jpg" alt="Color mask dust-filtered" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<img src="../images/cmask_dilate.jpg" alt="Color mask dilation" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<p style="clear: both;">
Figure 5. (left to right) Dust-filtered binary image and binary image with five dilation iterations
</div>

### Applying the mask
Once the color mask is created (e.g., Fig. 5, right), it is overlaid onto the original color image.
The masked color image is converted to grayscale based on the standard luminance equation.
The masked grayscale image is finally converted to binary, once again based on the Otsu method.

<div>
<img src="../images/cmask_mask_rgb.jpg" alt="Delta mask over RGB" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<img src="../images/cmask_mask_gray.jpg" alt="Delta mask grayscale" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<img src="../images/cmask_mask_bin.jpg" alt="Delta mask binary" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<p style="clear: both;">
Figure 6. (left to right) Original color image overlaid by the color mask, masked grayscale image, and masked binary image
</div>
