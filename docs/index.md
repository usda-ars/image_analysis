# Welcome to Plant Root Image Analysis
([return to plantmineralnutrition.net](http://plantmineralnutrition.net))

This documentation will guide you through the use of the Plant Root Image Analysis (PRIA) software.
This software represents the second stage of the Root System Architecture (RSA) digital imaging and analysis pipeline.
The purpose of this program is to prepare the digital images taken of root systems for trait measurement software.
During this stage, color images are converted to binary (i.e., black and white) in such a way as to represent the plant roots as true (i.e., white) against a false (i.e., black) background.
During the binarization process, the image foreground (i.e., root system) is partitioned from the background based on a variety of image-processing techniques.
