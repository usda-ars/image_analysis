# Experimental Setup for 3D Imaging

Digital image sequences are taken of root systems, grown in hydroponics, whereby the roots are rotated 360&deg; while suspended within a tower of mesh platforms.
Each platform is made of a thin anodized hexagonal aluminum ring.
The aluminum rings have post holes for aligning uprights and are notched around their periphery to allow the stringing of fishing line across the rings' openings a la tennis-racket style.
Presently, 30-pound test braided yellow fishing line is used for stringing the aluminum rings.
Three stainless steel rods provide vertical support for 8&ndash;12 mesh platforms stacked 2-6 cm apart offset by aluminum spacers and secured by collar clamps.
The resulting fishing-line mesh towers provide the substrata support during root growth while preserving, with modest respect, the root system's architecture (or as much root system architecture as can be preserved in hydroponics).

Germinated plant specimens are transplanted into the mesh towers and are grown in hydroponics (e.g., in a greenhouse or growth chamber) until they have reached the desired age for imaging.
When the time for imaging has come, the mesh towers are removed from hydroponics and are placed (one at a time) into a water-filled imaging tank.
Presently, the imaging tank is situated within a modular imaging station.
The imaging station serves as a self-contained dark room with lighting, camera and motor controls.
Images are captured and stored, along with all the relevant experimental details, in HDF5[^1] files.
[^1]: Hierarchical Data Format Version 5 (HDF5), The HDF Group, [https://www.hdfgroup.org](https://www.hdfgroup.org).

Before a 3D digital model can be generated for a root system from the imaging sequence, the pixels in each 2D image must first be partitioned between the foreground (i.e., the roots growing through the mesh) and background (i.e., everything that is not identified as roots).
To isolate the roots within each photograph, image processing techniques are employed.
