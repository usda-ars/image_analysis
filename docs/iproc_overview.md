# Image Processing for 3D Analysis

The main challenges faced with root identification and isolation from 2D images are contrast (i.e., the distinguishability of roots from the background), color (i.e., discontinuities due heterogeneous surface color of plant roots), and brightness (i.e., insufficient light collected by the camera).
While there are several techniques that may be utilized to address these challenges, this software focuses on the method of image masking.
An image mask is a pixel grid of zeros and ones that overlays a color image.
Masking the original image blacks out the colors where the mask is  zero and preserves the colors where the mask is one.
The challenge becomes defining a mask that shows the plant roots while hiding everything that is not a root.

This software employs two strategies for dealing with this problem.
