Nobuyuki Otsu (1979) A threshold selection method from gray-level histograms, *IEEE Transactions on Systems, Man, and Cybernetics*, *9*(1), 62&ndash;66.
