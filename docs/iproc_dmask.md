# Delta Mask
The delta mask formulae is an alternative technique to partition roots imaged in the 3D system.
The goal of the delta mask is to emphasize white regions of an image, which constitute primarily of the plant roots.
The processing steps are based, in part, on the equation for converting RGB to hue, saturation and luminance (HSL).

### Overview
1. Calculate max/min grayscale arrays
1. Calculate the range array
1. Binarize min and range array
1. Join min and range arrays
1. Blackout border
1. Remove dust
1. Dilate

### Color to max, min and range
The first processing step is to calculate the maximum (max) and minimum (min) arrays based on the largest and smallest RGB brightness value for each pixel in the original color image, respectively.
The range array is calculated based on the difference between the max and min arrays.

<div>
<img src="../images/example_img.jpg" alt="Original color image" style="float: left; width: 23%; margin-right: 1%; margin-bottom: 0.5em;">
<img src="../images/dmask_max.jpg" alt="Delta mask max" style="float: left; width: 23%; margin-right: 1%; margin-bottom: 0.5em;">
<img src="../images/dmask_min.jpg" alt="Delta mask min" style="float: left; width: 23%; margin-right: 1%; margin-bottom: 0.5em;">
<img src="../images/dmask_range.jpg" alt="Delta mask range" style="float: left; width: 23%; margin-right: 1%; margin-bottom: 0.5em;">
<p style="clear: both;">
Figure 1. (left to right) Color image; max array; min array; range array.
</div>

The min array, which represents the lowest brightness value of each pixel, mutes all colors except for whites and grays (i.e., where all three color bands are of equal brightness).
The range array emphasizes primary and secondary colors, where, of the three color bands, at least one band has a high and one band has a low brightness value.
It can be seen, therefore, that the min and range arrays highlight opposing features of a color image, which will be exploited for creating the delta mask.

### Binarize and join
The min and range arrays are converted to binary based on Otsu's method ([Otsu, 1979](references)).
The binary min and range arrays are then joined to form the mask array.

<div>
<img src="../images/dmask_rangeb.jpg" alt="Delta mask binary range" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<img src="../images/dmask_minb.jpg" alt="Delta mask binary min" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<img src="../images/dmask_join.jpg" alt="Delta mask join" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<p style="clear: both;">
Figure 2. (left to right) Binary range, min and mask arrays
</div>

### Border, filter and dilate
A border may be prescribed to manually black out regions surrounding the region of interest.
The border is defined by the relative extents of the region of interest, which by default spans from 0-100% from top to bottom and left to right.
Extents are relative to the top-left corner (0%, 0%).

Additionally, a dust-removal algorithm may be employed to further remove specks or unwanted noise from the region of interest.
The binarized and bordered image is scanned for white pixels.
White pixels are grouped into clusters based on a nearest-neighbor search algorithm.
Pixel clusters that are smaller than the prescribed size are removed from the binary image.

Finally, the mask may be expanded by applying a number of dilation iterations.
Each iteration grows the mask by a single pixel width.

<div>
<img src="../images/dmask_border.jpg" alt="Delta mask border" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<img src="../images/dmask_dust.jpg" alt="Delta mask dust removal" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<img src="../images/dmask_dilate.jpg" alt="Delta mask dilation" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<p style="clear: both;">
Figure 3. (left to right) Bordered binary image, dust-filtered binary image, and dilated binary image following five iterations
</div>

### Applying the mask
Once the delta mask is created (e.g., Fig. 3, right), it is overlaid onto the original color image.
The masked color image is converted to grayscale based on the standard luminance equation.
The masked grayscale image is finally converted to binary, once again based on the Otsu method.

<div>
<img src="../images/dmask_mask_rgb.jpg" alt="Delta mask over RGB" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<img src="../images/dmask_mask_gray.jpg" alt="Delta mask grayscale" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<img src="../images/dmask_mask_bin.jpg" alt="Delta mask binary" style="float: left; width: 30%; margin-right: 1%; margin-bottom: 0.5em;">
<p style="clear: both;">
Figure 4. (left to right) Original color image overlaid by the delta mask, masked grayscale image, and masked binary image
</div>
